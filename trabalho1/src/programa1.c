#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>

#include "lista.h"
#include "grafo.h"


void implementacaoMatrizAdj()
{
    
    //Matriz
    char ** matrizAdj;
    VerticeMatriz * listaVertices;
    int qtdVertices = 0, indexRaiz, i , j, verticePartida, qtdVerticesBusca = 0;
    int minutos, segundos;
    No * noRaiz;
    No * inicioFila;
    
    char * nomeArquivoEntrada [150];
    char * nomeArquivoSaida = "./arquivosDeSaida/saida.txt";
    struct timeval inicio, final;
    float tBuscaProfundidade, tBuscaLargura, tPartesConexas, tLeitura;
    printf("\n\nInforme o caminho do arquivo de entrada:\n");
    getchar();
    gets( nomeArquivoEntrada);
    
    printf("Iniciando array de vertices e matriz de adjacência Aguarde... \n");
    //Lendo quantidade de Vértices e inicializando vetor e matriz
    lerQuantidadeVertices(nomeArquivoEntrada, &qtdVertices);
    
    matrizAdj = (char **)calloc(qtdVertices, sizeof(char *));
        
    for(i = 0; i < qtdVertices; i++)
    {
        matrizAdj[i] = (char *)calloc(qtdVertices, sizeof(char *));
    }
    
    for(i = 0; i < qtdVertices; i++)
    {
        for(j = 0; j < qtdVertices; j++)
        {
            matrizAdj[i][j] = '0';
        }
    }
    
    listaVertices = (VerticeMatriz *)calloc(qtdVertices, sizeof(VerticeMatriz *));
    
    for(i = 0; i < qtdVertices; i++)
    {
        listaVertices[i].id = -1;
           
    }
    printf("Lendo o grafo Aguarde... \n");
    gettimeofday(&inicio, NULL);
    lerGrafoMatriz(nomeArquivoEntrada, nomeArquivoSaida, matrizAdj, listaVertices);
    gettimeofday(&final, NULL);
    tLeitura =  (final.tv_sec - inicio.tv_sec) ;
    minutos = tLeitura/60;
    segundos = ((int)tLeitura)%60;
    printf("O tempo de leitura do arquivo foi %dm:%ds\n", minutos, segundos );
    
    //--------------------------------------------------------------------
    
    int comando = 0;
    
    
    while(comando!=5)
    {
        printf("\n\nEscolha uma opção");
        printf("\n1 - realizar todas as operações");
        printf("\n2 - realizar busca em profundidade");
        printf("\n3 - realizar busca em Largura");
        printf("\n4 - realizar conta de partes conexas");
        printf("\n5 - Para Fechar o programa\n\n");
        //fflush(stdin);
        scanf("%d",&comando);
       
        switch(comando)
        {
            case 1:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("realizando operações Aguarde... \n");
                
                noRaiz = criarNo(-1);
                
                gettimeofday(&inicio, NULL);
                qtdVerticesBusca = 0;
                buscaProfundidadeMatriz("./arquivosDeSaida/buscaProfundidadeSaida.txt", indexRaiz, -1, listaVertices, &qtdVertices, matrizAdj,noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec);
                
                
                free(noRaiz);
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraMatriz("./arquivosDeSaida/buscaLarguraSaida.txt", indexRaiz, -1,listaVertices, &qtdVertices, matrizAdj, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                
                tBuscaLargura =  (final.tv_sec - inicio.tv_sec);
                
                free(noRaiz);
                noRaiz = criarNo(-1);
                gettimeofday(&inicio, NULL);
                verificarPartesConexasMatriz("./arquivosDeSaida/partesConexas.txt", listaVertices, &qtdVertices, matrizAdj);
                gettimeofday(&final, NULL);
                
                tPartesConexas = (final.tv_sec - inicio.tv_sec);
                
                
                free(noRaiz);
                free(inicioFila);
                
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
            }
            case 2:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                qtdVerticesBusca = 0;
                
                gettimeofday(&inicio, NULL);
                buscaProfundidadeMatriz("./arquivosDeSaida/buscaProfundidadeSaida.txt", indexRaiz, -1, listaVertices, &qtdVertices, matrizAdj,noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec) ;
                                
                free(noRaiz);
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );

                break;
            }
            case 3:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraMatriz("./arquivosDeSaida/buscaLarguraSaida.txt", indexRaiz, -1,listaVertices, &qtdVertices, matrizAdj, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                
                tBuscaLargura = (final.tv_sec - inicio.tv_sec) ;
                
                free(noRaiz);
                free(inicioFila);
                
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                                
                break;
            }
            case 4:
            {
                
                gettimeofday(&inicio, NULL);
                verificarPartesConexasMatriz("./arquivosDeSaida/partesConexas.txt", listaVertices, &qtdVertices, matrizAdj);
                gettimeofday(&final, NULL);
                
                tPartesConexas =  (final.tv_sec - inicio.tv_sec) ;
                
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
                
            }
            case 5:
                continue;
                break;
             
            default:
              printf("\n\nNenhuma opcao foi escolhida.");
        }
        getchar();
        
    }
    
    for(i = 0; i < qtdVertices; i++)
    {
        free(matrizAdj[i]);
    }
    
    free(listaVertices);
    free(matrizAdj);
    
    
}

void implementacaoListaAdj(){
    
    //Lista
    VerticeLista* listaVertices;
    int qtdVertices = 0, indexRaiz, i, comando = 0, verticePartida, qtdVerticesBusca = 0;
    No* aux;
    No * noRaiz  = criarNo(-1);
    No * inicioFila  = criarNo(-1);
    char * nomeArquivoEntrada [150];
    char * nomeArquivoSaida = "./arquivosDeSaida/saidaLista.txt";
    struct timeval inicio, final;
    float tBuscaProfundidade, tBuscaLargura, tPartesConexas, tLeitura;
    int minutos, segundos;
    
    printf("\n\nInforme o caminho do arquivo de entrada:\n");
    fflush(stdin);
    getchar();
    gets(nomeArquivoEntrada);
    
    printf("Iniciandi array de vertices Aguarde... \n");
    lerQuantidadeVertices(nomeArquivoEntrada, &qtdVertices);
    
    listaVertices = (VerticeLista *)calloc(qtdVertices, sizeof(VerticeLista));
    
    
    for(i = 0; i < qtdVertices; i++)
    {
        
        listaVertices[i].id = -1;
        listaVertices[i].grau = 0;
        listaVertices[i].nivel = 0;
        aux = criarNo(-1);
        listaVertices[i].aresta = aux;
           
    }
    
    printf("Lendo o grafo Aguarde... \n");
    gettimeofday(&inicio, NULL);
    lerGrafoLista(nomeArquivoEntrada, nomeArquivoSaida,listaVertices);
    gettimeofday(&final, NULL);
    tLeitura =  (final.tv_sec - inicio.tv_sec) ;
    minutos = tLeitura/60;
    segundos = ((int)tLeitura)%60;
    printf("O tempo de tLeitura do arquivo foi %dm:%ds\n", minutos, segundos );
    
    
    //------------------------------------------------------------------------
    
    while(comando!=5)
    {
        printf("\n\nEscolha uma opção");
        printf("\n1 - realizar todas as operações");
        printf("\n2 - realizar busca em profundidade");
        printf("\n3 - realizar busca em Largura");
        printf("\n4 - realizar conta de partes conexas");
        printf("\n5 - Para Fechar o programa\n\n");
        //fflush(stdin);
        scanf("%d",&comando);
       
        switch(comando)
        {
            case 1:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("realizando operações Aguarde... \n");
                
                noRaiz = criarNo(-1);
                qtdVerticesBusca = 0;
                
                gettimeofday(&inicio, NULL);
                buscaProfundidadeLista("./arquivosDeSaida/buscaProfundidadeSaidaLista.txt", indexRaiz,-1, listaVertices, &qtdVertices, noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec);
                
                
                free(noRaiz);
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraLista("./arquivosDeSaida/buscaLarguraSaidaLista.txt", indexRaiz, -1,listaVertices, &qtdVertices, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                free(noRaiz);
                free(inicioFila);
                
                tBuscaLargura =  (final.tv_sec - inicio.tv_sec);
                
                gettimeofday(&inicio, NULL);
                verificarPartesConexasLista("./arquivosDeSaida/partesConexasLista.txt", listaVertices, &qtdVertices);
                gettimeofday(&final, NULL);
                
                tPartesConexas = (final.tv_sec - inicio.tv_sec);
                
                
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
            }
            case 2:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                qtdVerticesBusca = 0;
                gettimeofday(&inicio, NULL);
                buscaProfundidadeLista("./arquivosDeSaida/buscaProfundidadeSaidaLista.txt", indexRaiz,-1, listaVertices, &qtdVertices, noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec) ;
                                
                free(noRaiz);
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );

                break;
            }
            case 3:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraLista("./arquivosDeSaida/buscaLarguraSaidaLista.txt", indexRaiz, -1,listaVertices, &qtdVertices, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                
                tBuscaLargura = (final.tv_sec - inicio.tv_sec) ;
                
                free(noRaiz);
                free(inicioFila);
                
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                                
                break;
            }
            case 4:
            {
                
                gettimeofday(&inicio, NULL);
                verificarPartesConexasLista("./arquivosDeSaida/partesConexasLista.txt", listaVertices, &qtdVertices);
                gettimeofday(&final, NULL);
                
                tPartesConexas =  (final.tv_sec - inicio.tv_sec) ;
                
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
                
            }
            case 5:
                continue;
                break;
             
            default:
              printf("\n\nNenhuma opcao foi escolhida.");
        }
        getchar();
        
    }
    free(listaVertices);
    
}

int main(int argc, char ** argv)
{
    int comando = 0;
    
    while(comando!=3)
    {
        printf("\n\nEscolha o tipo de representação");
        printf("\n1 - Matriz de adjacência");
        printf("\n2 - Lista de adjacência");
        printf("\n3 - Para Fechar o programa\n\n");
        fflush(stdin);
        
        scanf("%d",&comando);
       
        switch(comando)
        {
            case 1:
                implementacaoMatrizAdj();
                break;
            case 2:
                implementacaoListaAdj();
                break;
            case 3:
                continue;
                break;
             
            default:
              printf("\n\nNenhuma opcao foi escolhida.");
        }
        
        getchar();
        
        
    }
    
    return 0;
}