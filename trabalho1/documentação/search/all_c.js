var searchData=
[
  ['verificarpartesconexaslista',['verificarPartesConexasLista',['../grafo_8h.html#a72d15b9cb56e4f03dcbf1fd844a749cb',1,'verificarPartesConexasLista(char *paramNomeArquivoSaida, VerticeLista *paramListaVertices, int *paramQtdVertices):&#160;grafo.c'],['../grafo_8c.html#a72d15b9cb56e4f03dcbf1fd844a749cb',1,'verificarPartesConexasLista(char *paramNomeArquivoSaida, VerticeLista *paramListaVertices, int *paramQtdVertices):&#160;grafo.c']]],
  ['verificarpartesconexasmatriz',['verificarPartesConexasMatriz',['../grafo_8h.html#a9acc6bb95bee4c0a66ecd4072aa752ed',1,'verificarPartesConexasMatriz(char *paramNomeArquivoSaida, VerticeMatriz *paramListaVertices, int *paramQtdVertices, char **paramMatrizAdj):&#160;grafo.c'],['../grafo_8c.html#a9acc6bb95bee4c0a66ecd4072aa752ed',1,'verificarPartesConexasMatriz(char *paramNomeArquivoSaida, VerticeMatriz *paramListaVertices, int *paramQtdVertices, char **paramMatrizAdj):&#160;grafo.c']]],
  ['verticelista',['VerticeLista',['../structVerticeLista.html',1,'VerticeLista'],['../grafo_8h.html#a0e4b9a66facfc7b224e9888828c2fdde',1,'VerticeLista():&#160;grafo.h']]],
  ['verticematriz',['VerticeMatriz',['../structVerticeMatriz.html',1,'VerticeMatriz'],['../grafo_8h.html#a59f9ea611feb09b16d16a6678c584eaf',1,'VerticeMatriz():&#160;grafo.h']]]
];
