var indexSectionsWithContent =
{
  0: "_abcgilmnprtv",
  1: "nv",
  2: "glpt",
  3: "bcilmprv",
  4: "acginp",
  5: "nv",
  6: "_p"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines"
};

var indexSectionLabels =
{
  0: "Todos",
  1: "Classes",
  2: "Arquivos",
  3: "Funções",
  4: "Variáveis",
  5: "Definições de Tipos",
  6: "Definições e Macros"
};

