#ifndef LISTA_HEADER_
#define LISTA_HEADER_
typedef struct No{
    /** Identificador do No*/
    int id; 
    /** Ponteiro para o próximo No da lista*/
    struct No * prox;
} No;

No* criarNo(int id);
void inserirInicioLista(No* noCabeca, No* novoNo);
void inserirFinalLista(No* noCabeca, No* novoNo);
No* pesquisarNo(No* noCabeca, int idBusca);
No* removerNoInicio(No* noCabeca);

#endif