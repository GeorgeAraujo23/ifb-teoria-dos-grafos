#ifndef GRAFO_HEADER_
#define GRAFO_HEADER_

typedef struct VerticeMatriz
{
    /**Identificador do Vértice*/
    int id;
    /** Indica o grau do Vértice*/
    unsigned short int  grau;
    /** indica o nível do Vértice nas buscaLarguraMatriz e buscaProfundidadeMatriz*/
    unsigned short int nivel;
}VerticeMatriz;

typedef struct VerticeLista
{
    /** Identificador do Vertice*/
    int id;
    /** Indica o grau do Vértice*/
    unsigned short int  grau;
    /** indica o nível do Vértice nas buscaLarguraLista e buscaProfundidadeLista*/
    unsigned short int nivel;
    /** Ponteiro para o No cadeça da lista de adjacência*/
    No* aresta;
}VerticeLista;

void imprimirVerticesMatriz(VerticeMatriz * paramListaVertices, int * paramMaxIndice);

void imprimirMatrizAdjacencia(char ** paramMatrizAdj, int * paramQtdVertices);

int buscarVerticeMatriz(VerticeMatriz * paramListaVertices, int paramVertice, int *paramMaxIndice);

int inserirVerticeMatriz(VerticeMatriz * paramListaVertices, int paramVertice, int * paramMaxIndice);

void buscaLarguraMatriz(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeMatriz * paramListaVertices, int *paramQtdVertices, char ** paramMatrizAdj,No * paramNosVisitados, No * paramInicioFila);

void buscaProfundidadeMatriz(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeMatriz * paramListaVertices, int* paramQtdVertices, char ** paramMatrizAdj, No * paramNoRaiz, int paramNivel, int* paramQtdVerticesBusca);

void lerGrafoMatriz(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,char ** paramMatrizAdj, VerticeMatriz * paramListaVertices);

void verificarPartesConexasMatriz(char* paramNomeArquivoSaida, VerticeMatriz * paramListaVertices, int* paramQtdVertices, char ** paramMatrizAdj);

void imprimirVerticesLista(VerticeLista * paramListaVertices, int * paramMaxIndice);

int buscarVerticeLista(VerticeLista * paramListaVertices, int paramVertice, int *paramMaxIndice);

int inserirVerticeLista(VerticeLista* paramListaVertices, int paramVertice, int * paramMaxIndice);

void buscaLarguraLista(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeLista * paramListaVertices, int *paramQtdVertices,No * paramNosVisitados, No * paramInicioFila);

void buscaProfundidadeLista(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeLista * paramListaVertices, int* paramQtdVertices, No * paramNoRaiz, int paramNivel, int* paramQtdVerticesBusca);

void lerGrafoLista(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,VerticeLista* paramListaVertices);

void verificarPartesConexasLista(char* paramNomeArquivoSaida, VerticeLista * paramListaVertices, int* paramQtdVertices);

void lerQuantidadeVertices(char * paramNomeArquivoEntrada, int* paramQtdVertices);

#endif