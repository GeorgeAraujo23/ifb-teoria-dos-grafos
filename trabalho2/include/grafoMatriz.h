#ifndef GRAFO_MATRIZ_HEADER_
#define GRAFO_MATRIZ_HEADER_

typedef struct VerticeMatriz
{
    /**Identificador do Vértice*/
    int id;
    /** Indica o grau do Vértice*/
    unsigned short int  grau;
    /** indica o nível do Vértice nas buscaLarguraMatriz e buscaProfundidadeMatriz*/
    unsigned short int nivel;
    /**Identificador do index do vértice antecessor*/
    int indexAntecessor;
}VerticeMatriz;

void imprimirVerticesMatriz(VerticeMatriz * paramListaVertices, int * paramMaxIndice);

void imprimirMatrizAdjacencia(char ** paramMatrizAdj, int * paramQtdVertices);

int buscarVerticeMatriz(VerticeMatriz * paramListaVertices, int paramVertice, int *paramMaxIndice);

int inserirVerticeMatriz(VerticeMatriz * paramListaVertices, int paramVertice, int * paramMaxIndice);

void buscaLarguraMatriz(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeMatriz * paramListaVertices, int *paramQtdVertices, char ** paramMatrizAdj,No * paramNosVisitados, No * paramInicioFila);

void buscaProfundidadeMatriz(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeMatriz * paramListaVertices, int* paramQtdVertices, char ** paramMatrizAdj, No * paramNoRaiz, int paramNivel, int* paramQtdVerticesBusca);

void lerGrafoMatriz(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,char ** paramMatrizAdj, VerticeMatriz * paramListaVertices);

void verificarPartesConexasMatriz(char* paramNomeArquivoSaida, VerticeMatriz * paramListaVertices, int* paramQtdVertices, char ** paramMatrizAdj);

void caminhoEntreVerticesMatriz(int paramIndexOrigem, int paramIndexDestino, VerticeMatriz * paramListaVertices, int* paramQtdVertices);

void prepararCalculoCaminhoMatriz(VerticeMatriz * paramListaVertices, int* paramQtdVertices);

#endif