#ifndef GRAFO_LISTA_HEADER_
#define GRAFO_LISTA_HEADER_

typedef struct VerticeLista
{
    /** Identificador do Vertice*/
    int id;
    /** Indica o grau do Vértice*/
    unsigned short int  grau;
    /** indica o nível do Vértice nas buscaLarguraLista e buscaProfundidadeLista*/
    unsigned short int nivel;
    /** Ponteiro para o No cadeça da lista de adjacência*/
    No* aresta;
//     /**Identificador do index do vértice antecessor*/
    int indexAntecessor;
}VerticeLista;

void imprimirVerticesLista(VerticeLista * paramListaVertices, int * paramMaxIndice);

int buscarVerticeLista(VerticeLista * paramListaVertices, int paramVertice, int *paramMaxIndice);

int inserirVerticeLista(VerticeLista* paramListaVertices, int paramVertice, int * paramMaxIndice);

void buscaLarguraLista(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeLista * paramListaVertices, int *paramQtdVertices,No * paramNosVisitados, No * paramInicioFila);

void buscaProfundidadeLista(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeLista * paramListaVertices, int* paramQtdVertices, No * paramNoRaiz, int paramNivel, int* paramQtdVerticesBusca);

void lerGrafoLista(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,VerticeLista* paramListaVertices);

void verificarPartesConexasLista(char* paramNomeArquivoSaida, VerticeLista * paramListaVertices, int* paramQtdVertices);

void lerQuantidadeVertices(char * paramNomeArquivoEntrada, int* paramQtdVertices);

void caminhoEntreVerticesLista(int paramIndexOrigem, int paramIndexDestino, VerticeLista * paramListaVertices, int* paramQtdVertices);

void prepararCalculoCaminhoLista(VerticeLista * paramListaVertices, int* paramQtdVertices);

#endif