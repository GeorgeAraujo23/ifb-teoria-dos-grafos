#ifndef GRAFO_PONDERADO_LISTA_HEADER_
#define GRAFO_PONDERADO_LISTA_HEADER_

typedef struct VerticeListaP
{
    /** Identificador do Vertice*/
    int id;
    /** Indica o grau do Vértice*/
    unsigned short int  grau;
    /** indica o nível do Vértice nas buscaLarguraLista e buscaProfundidadeLista*/
    unsigned short int nivel;
    /** Ponteiro para o No cadeça da lista de adjacência*/
    NoP* aresta;
    /**Identificador se o vértice foi ou não fechado (usado para o caminhoDijkstraLista)*/
    char fechado;
    /**Identificador do index do vértice antecessor (usado para o caminhoDijkstraLista)*/
    int indexAntecessor;
    /**Identificador do peso das arestas a partir do vertice incial  (usado para o caminhoDijkstraLista)*/
    float peso;
}VerticeListaP;

void imprimirVerticesListaP(VerticeListaP * paramListaVertices, int * paramMaxIndice);

int buscarVerticeListaP(VerticeListaP * paramListaVertices, int paramVertice, int *paramMaxIndice);

int inserirVerticeListaP(VerticeListaP* paramListaVertices, int paramVertice, int * paramMaxIndice);

void buscaLarguraListaP(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeListaP * paramListaVertices, int *paramQtdVertices,NoP * paramNosVisitados, NoP * paramInicioFila);

void buscaProfundidadeListaP(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeListaP * paramListaVertices, int* paramQtdVertices, NoP * paramNoRaiz, int paramNivel, int* paramQtdVerticesBusca);

int lerGrafoListaP(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,VerticeListaP* paramListaVertices);

void verificarPartesConexasListaP(char* paramNomeArquivoSaida, VerticeListaP * paramListaVertices, int* paramQtdVertices);

void caminhoDijkstraLista(int paramIndexOrigem, int paramIndexDestino, VerticeListaP * paramListaVertices, int* paramQtdVertices);

#endif
