#ifndef GRAFO_PONDERADO_MATRIZ_HEADER_
#define GRAFO_PONDERADO_MATRIZ_HEADER_

typedef struct VerticeMatrizP
{
    /**Identificador do Vértice*/
    int id;
    /** Indica o grau do Vértice*/
    unsigned short int  grau;
    /** indica o nível do Vértice nas buscaLarguraMatriz e buscaProfundidadeMatriz*/
    unsigned short int nivel;
    /**Identificador se o vértice foi ou não fechado (usado para o caminhoDijkstraMatriz)*/
    char fechado;
    /**Identificador do index do vértice antecessor (usado para o caminhoDijkstraMatriz)*/
    int indexAntecessor;
    /**Identificador do peso das arestas a partir do vertice incial  (usado para o caminhoDijkstraMatriz)*/
    float peso;
}VerticeMatrizP;

void imprimirVerticesMatrizP(VerticeMatrizP * paramListaVertices, int * paramMaxIndice);

void imprimirMatrizAdjacenciaP(float ** paramMatrizAdj, int * paramQtdVertices);

int buscarVerticeMatrizP(VerticeMatrizP * paramListaVertices, int paramVertice, int *paramMaxIndice);

int inserirVerticeMatrizP(VerticeMatrizP * paramListaVertices, int paramVertice, int * paramMaxIndice);

void buscaLarguraMatrizP(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeMatrizP * paramListaVertices, int *paramQtdVertices, float ** paramMatrizAdj,NoP * paramNosVisitados, NoP * paramInicioFila);

void buscaProfundidadeMatrizP(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeMatrizP * paramListaVertices, int* paramQtdVertices, float ** paramMatrizAdj, NoP * paramNoRaiz, int paramNivel, int* paramQtdVerticesBusca);

int lerGrafoMatrizP(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,float ** paramMatrizAdj, VerticeMatrizP * paramListaVertices, int* paramQtdVertices);

void caminhoDijkstraMatriz(int paramIndexOrigem, int paramIndexDestino,float ** paramMatrizAdj, VerticeMatrizP * paramListaVertices, int* paramQtdVertices);

void verificarPartesConexasMatrizP(char* paramNomeArquivoSaida, VerticeMatrizP * paramListaVertices, int* paramQtdVertices, float ** paramMatrizAdj);

#endif
