#ifndef LISTA_PONDERADA_HEADER_
#define LISTA_PONDERADA_HEADER_
typedef struct NoP{
    /** Identificador do NoPonderado*/
    int id; 
    /** Indicador do peso do nó*/
    float peso;
    /** Ponteiro para o próximo NoPonderado da lista*/
    struct NoP * prox;
} NoP;

NoP* criarNoP(int id, float peso);
void inserirInicioListaP(NoP* noCabeca, NoP* novoNo);
void inserirInicioListaOrdenadaP(NoP* noCabeca, NoP* novoNo);
void inserirFinalListaP(NoP* noCabeca, NoP* novoNo);
NoP* pesquisarNoP(NoP* noCabeca, int idBusca);
NoP* removerNoInicioP(NoP* noCabeca);
NoP* removerNoP(NoP* noCabeca, int idBusca);
void imprimirListaP(NoP* noCabeca);

#endif