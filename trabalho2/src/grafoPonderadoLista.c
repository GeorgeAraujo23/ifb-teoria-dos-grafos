/**
 * @brief "Esse código implementa uma biblioteca para grafos, ponderados e não dirigidos, usando Lista de adjacência"
 * @file grafoPonderadoLista.c
 * @author George Araújo - george.ifrn@gmail.com
 * @since 22/11/2016
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include "listaPonderada.h"
#include "grafoPonderadoLista.h"

/**Função imprime identificador de todos os Vértices do paramListaVertices e sua lista de adjacência
 * @param paramListaVertices uma referência para um vertor de VerticeLista
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return void
 */
void imprimirVerticesListaP(VerticeListaP * paramListaVertices, int * paramMaxIndice)
{
    NoP* aux;
    for(int i = 0; i < *paramMaxIndice; i++)
    {
        printf("%d -> ", paramListaVertices[i].id);
        aux = paramListaVertices[i].aresta;
        while(aux->prox != NULL)
        {
            aux = aux->prox;
            printf("%d ->", paramListaVertices[aux->id].id);
        }
        printf("\n");
    }
}

/**Função busca um VerticeLista no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeLista
 * @param paramVertice um inteiro com o identificador do VerticeLista procurado
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com valor -1 caso não tenha encontrado o VerticeListaP ou  o index do vetor na posição onde o VerticeListaP se encontra
 */
int buscarVerticeListaP(VerticeListaP * paramListaVertices, int paramVertice, int *paramMaxIndice)
{
    
    for(int i = 0; i <= *paramMaxIndice; i++)
    {
        if(paramListaVertices[i].id == paramVertice)
        {
            return i;
            
        }
    }
    
    return -1;
    
}

/**Função insere um VerticeLista no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeListaP
 * @param paramVertice um inteiro com o identificador do novo VerticeListaP
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com o novo tamanho usado do vetor paramListaVertices
 */
int inserirVerticeListaP(VerticeListaP* paramListaVertices, int paramVertice, int * paramMaxIndice)
{
    
    *paramMaxIndice = *paramMaxIndice + 1;
    
    paramListaVertices[*paramMaxIndice].id = paramVertice;
    paramListaVertices[*paramMaxIndice].grau = 0;
    paramListaVertices[*paramMaxIndice].nivel = 0;
    paramListaVertices[*paramMaxIndice].aresta = criarNoP(-1, -1);
    paramListaVertices[*paramMaxIndice].fechado = 'N';
    paramListaVertices[*paramMaxIndice].peso = -1.0;
    paramListaVertices[*paramMaxIndice].indexAntecessor = -1;
    return *paramMaxIndice;
}

/**Função faz uma busca em largura no grafo a partir de um VerticeListaP
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeListaP que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeListaP pai
 * @param paramListaVertices uma referência para um vertor de VerticeListaP
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeListaP já visitados
 * @param paramInicioFila um No cabeça para uma fila que indica ordem dos VerticeListaP que devem ser visitados
 * @sa criarNoP inserirInicioListaP inserirFinalListaP removerNoInicioP pesquisarNoP
 * @return
 */
void buscaLarguraListaP(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeListaP * paramListaVertices, int *paramQtdVertices,NoP * paramNosVisitados, NoP * paramInicioFila)
{
    NoP *noVisitado;
    NoP *noAuxFila;
    NoP *novoNoFila;
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaLarguraLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {
        if(paramIndexPai == -1)
        {
            paramListaVertices[paramIndexRaiz].nivel = 0;
            noVisitado = criarNoP(paramIndexRaiz,-1);
            inserirInicioListaP(paramNosVisitados, noVisitado);
            fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, 0, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        }
        
        noAuxFila = paramListaVertices[paramIndexRaiz].aresta;
        while(noAuxFila->prox != NULL)
        {
            noAuxFila = noAuxFila->prox;
            
            if(pesquisarNoP(paramNosVisitados, noAuxFila->id) == NULL)
            {    
                noVisitado = criarNoP(noAuxFila->id,-1);
                inserirInicioListaP(paramNosVisitados, noVisitado);

                novoNoFila = criarNoP(noAuxFila->id,-1);
                inserirFinalListaP(paramInicioFila, novoNoFila);
                
                paramListaVertices[noAuxFila->id].nivel = paramListaVertices[paramIndexRaiz].nivel + 1;
                
                fprintf(arquivoSaida, "%d %d %d \n", paramListaVertices[noAuxFila->id].id,paramListaVertices[noAuxFila->id].nivel, paramListaVertices[paramIndexRaiz].id);
            }
                    
            
        }
        
        fclose(arquivoSaida);
        
        if(paramInicioFila->prox != NULL)
        {   
            noAuxFila = removerNoInicioP(paramInicioFila);
            buscaLarguraListaP(paramNomeArquivoSaida, noAuxFila->id, paramIndexRaiz,paramListaVertices, paramQtdVertices, paramNosVisitados, paramInicioFila);  
            
        }
    }
    
}

/**Função faz uma busca em profundidade no grafo a partir de um VerticeLista
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeListaP que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeListaP pai
 * @param paramListaVertices uma referência para um vertor de VerticeListaP
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeListaP já visitados
 * @param paramNivel um inteiro que indica o nivel do VerticeListaP pai
 * @sa criarNoP inserirInicioListaP pesquisarNoP
 * @return
 */
void buscaProfundidadeListaP(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeListaP * paramListaVertices, int* paramQtdVertices, NoP * paramNosVisitados, int paramNivel, int* paramQtdVerticesBusca)
{
    NoP *noVisitado = criarNoP(paramIndexRaiz,-1);
    NoP *noAux;
    inserirInicioListaP(paramNosVisitados, noVisitado);
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaProfundidadeLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, paramNivel, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        *paramQtdVerticesBusca = *paramQtdVerticesBusca + 1;
        fclose(arquivoSaida);
        paramNivel++;
        noAux = paramListaVertices[paramIndexRaiz].aresta;
            
        while(noAux->prox != NULL)
        {
            noAux = noAux->prox;
            if(pesquisarNoP(paramNosVisitados, noAux->id) == NULL){
                buscaProfundidadeListaP(paramNomeArquivoSaida, noAux->id,paramIndexRaiz, paramListaVertices, paramQtdVertices, paramNosVisitados, paramNivel, paramQtdVerticesBusca);
            }
        }
    }
}

/**Função lê o arquivo de entrada com o grafo e gera o vetor de VerticeListaP
 * @param paramNomeArquivoEntrada nome do arquivo de entrada do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida do grafo
 * @param paramListaVertices uma referência para um vertor de VerticeListaP onde seŕão armazenados os vértices
 * @sa buscarVerticeListaP inserirVerticeListaP inserirFinalListaP
 * @return inteiro indicando se houve erro na função, retorna 1 se a leitura for feita com sucesso e 0 se algum peso do grafo for menor que zero
 */
int lerGrafoListaP(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,VerticeListaP* paramListaVertices)
{
    int resultado = 1;
    FILE * entrada, * saida;
    float peso;
    int vertice1, vertice2, indexVertice1, indexVertice2, maxIndice = -1, qtdVertices = 0, qtdArestas = 0;
    NoP* noAux;
    entrada = fopen(paramNomeArquivoEntrada, "r");
    if(entrada == NULL)
    {
        printf("Método: lerGrafoLista - Erro na leitura do arquivo %s\n", paramNomeArquivoEntrada);
        exit(0);
    }else
    {
        fscanf(entrada, "%d",&qtdVertices);
        
        while(!feof(entrada))
        {
            fscanf(entrada, "%d %d %f", &vertice1, &vertice2, &peso);
            
            
            indexVertice1 = buscarVerticeListaP(paramListaVertices, vertice1, &maxIndice);
            if(indexVertice1 == -1)
            {
                indexVertice1 = inserirVerticeListaP(paramListaVertices, vertice1, &maxIndice);
            }
            
            indexVertice2 = buscarVerticeListaP(paramListaVertices, vertice2, &maxIndice);
            if(indexVertice2 == -1)
            {
                indexVertice2 = inserirVerticeListaP(paramListaVertices, vertice2, &maxIndice);
            }
            
            noAux = criarNoP(indexVertice1 , peso);
            inserirFinalListaP(paramListaVertices[indexVertice2].aresta, noAux);
            noAux = criarNoP(indexVertice2, peso);
            inserirFinalListaP(paramListaVertices[indexVertice1].aresta, noAux);
                        
            qtdArestas++;
            paramListaVertices[indexVertice2].grau++;
            paramListaVertices[indexVertice1].grau++;
            
            if(peso < 0.0)
            {
                resultado = 0;
            }
        }
        
        fclose(entrada);
        entrada = NULL;
        free(entrada);
        maxIndice++;
        qtdArestas--;
        
        saida = fopen(paramNomeArquivoSaida, "w");
        fprintf(saida, "N = %d\n", maxIndice);
        fprintf(saida, "M = %d\n", qtdArestas);
        
        for(int i = 0; i < maxIndice; i++)
        {
            fprintf(saida, "%d %d\n", paramListaVertices[i].id, paramListaVertices[i].grau);                    
        }
        
        fclose(saida); 
        saida = NULL;
        free(saida);
        
    }
    return resultado;
}

/**Função implementa o algoritmo de Dijkstra para caminho mínimo
 * @param paramIndexOrigem index do vértice de partida do caminho
 * @param paramIndexDestino index do vértice de chegada do caminho
 * @param paramListaVertices uma referência para um vertor de VerticeListaP onde seŕão armazenados os vérticesparamQtdVertices
 * @param paramQtdVertices indica a quantidade de vértices do grafo
 * @sa inserirInicioListaOrdenadaP removerNoInicioP criarNoP pesquisarNoP removerNoP inserirInicioListaP
 * @return
 */
void caminhoDijkstraLista(int paramIndexOrigem, int paramIndexDestino, VerticeListaP * paramListaVertices, int* paramQtdVertices)
{
    for(int h = 0; h <= *paramQtdVertices; h++)
    {
        paramListaVertices[h].fechado = 'N';
        paramListaVertices[h].peso = -1.0;
        paramListaVertices[h].indexAntecessor = -1;
    }
    
    FILE * arquivoSaida;
    paramListaVertices[paramIndexOrigem].fechado = 'N';
    paramListaVertices[paramIndexOrigem].peso = 0.0;
    paramListaVertices[paramIndexOrigem].indexAntecessor = -1.0;
    NoP * NoPesquisa;
    NoP * NoRaiz = criarNoP(-1, -1);
    NoP* novoNo  = criarNoP(paramIndexOrigem, 0.0);
    inserirInicioListaOrdenadaP(NoRaiz, novoNo);
    
    while(NoRaiz->prox != NULL)
    {
        int j = NoRaiz->prox->id;
        float peso = NoRaiz->prox->peso;
        removerNoInicioP(NoRaiz);
        paramListaVertices[j].fechado = 'S';
        paramListaVertices[j].fechado = 'S';
        NoP* arestaAuxiliar = paramListaVertices[j].aresta->prox;
        while(arestaAuxiliar != NULL)
        {
            if(paramListaVertices[arestaAuxiliar->id].fechado == 'N'){
                
                if(paramListaVertices[arestaAuxiliar->id].peso == -1 || paramListaVertices[arestaAuxiliar->id].peso > arestaAuxiliar->peso + peso)
                {       
                    paramListaVertices[arestaAuxiliar->id].indexAntecessor = j;
                    paramListaVertices[arestaAuxiliar->id].peso = arestaAuxiliar->peso + paramListaVertices[j].peso;
                    
                    novoNo = criarNoP(arestaAuxiliar->id, paramListaVertices[arestaAuxiliar->id].peso);
                    NoPesquisa = pesquisarNoP(NoRaiz, arestaAuxiliar->id);
                    
                    if(NoPesquisa != NULL){
                        removerNoP(NoRaiz, NoPesquisa);
                    }
                    inserirInicioListaOrdenadaP(NoRaiz, novoNo);
                }
            }
                
            arestaAuxiliar = arestaAuxiliar->prox;
        }
    }    
    
    NoP* NoSolucao = criarNoP(-1, -1);
    int indexAntecessor = paramIndexDestino;
    NoP* auxiliar = criarNoP(indexAntecessor, paramListaVertices[indexAntecessor].peso);
    inserirInicioListaP(NoSolucao, auxiliar);
    indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    
    while(indexAntecessor != -1.0){
        
        auxiliar = criarNoP(indexAntecessor, paramListaVertices[indexAntecessor].peso);
        inserirInicioListaP(NoSolucao, auxiliar);
        indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    }
    
    arquivoSaida = fopen("./arquivosDeSaida/dijkstraLista.txt", "a");
    fprintf(arquivoSaida, "Distância do caminho: %.2f\n", paramListaVertices[paramIndexDestino].peso);
    auxiliar = NoSolucao->prox;
    
    while(auxiliar != NULL){
        
        fprintf(arquivoSaida, "%d %.2f %d \n", paramListaVertices[auxiliar->id].id, paramListaVertices[auxiliar->id].peso, paramListaVertices[auxiliar->id].indexAntecessor == -1.0 ? 0 : paramListaVertices[paramListaVertices[auxiliar->id].indexAntecessor].id);
        auxiliar = auxiliar->prox;
    }
    
    fclose(arquivoSaida);    
}


/**Função verfica as partes conexas do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida com as partes conexas
 * @param paramListaVertices uma referência para um vertor de VerticeListaP onde seŕão armazenados os vértices
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa pesquisarNoP buscaProfundidadeListaP
 * @return
 */
void verificarPartesConexasListaP(char* paramNomeArquivoSaida, VerticeListaP * paramListaVertices, int* paramQtdVertices)
{
    NoP* noConexos = criarNoP(-1,-1);
    int countConexos = 0, maxParteConexa = 0, minParteConexa = 0, maxQtdVerticeParteConexa = 0, minQtdVerticeParteConexa = *paramQtdVertices;
    FILE* saida;
    int qtdVerticesBusca = 0;
    
    for(int i = 0; i < *paramQtdVertices; i++)
    {
        if(pesquisarNoP(noConexos, i)  == NULL)
        {
            countConexos++;
            qtdVerticesBusca = 0;
            buscaProfundidadeListaP(paramNomeArquivoSaida, i, -1, paramListaVertices, paramQtdVertices, noConexos, 0, &qtdVerticesBusca);
            
            if(maxQtdVerticeParteConexa < qtdVerticesBusca){
                maxQtdVerticeParteConexa = qtdVerticesBusca;
                maxParteConexa = countConexos;
            }
            
            if(minQtdVerticeParteConexa > qtdVerticesBusca){
                minQtdVerticeParteConexa = qtdVerticesBusca;
                minParteConexa = countConexos;
            }
            
            saida = fopen(paramNomeArquivoSaida, "a");
            if(saida == NULL)
            {
                printf("Método: verificarPartesConexasLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
                exit(0);
            }else
            {    
                fprintf(saida, "Parte: %d Quantidade de vértices: %d\n", countConexos, qtdVerticesBusca);
                fclose(saida);
            }
        }
    }
    
    saida = fopen(paramNomeArquivoSaida, "a");
    if(saida == NULL)
    {
        printf("Método: verificarPartesConexasLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(saida, "Quantidade de partes conexas %d\n", countConexos);
        fprintf(saida, "Maior Parte: %d Quantidade de Vértices %d\n",maxParteConexa ,maxQtdVerticeParteConexa);
        fprintf(saida, "Menor Parte: %d Quantidade de Vértices %d\n",minParteConexa ,minQtdVerticeParteConexa);
        fclose(saida);
    }
}