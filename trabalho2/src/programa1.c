#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/time.h>

#include "lista.h"
#include "listaPonderada.h"
#include "grafoMatriz.h"
#include "grafoLista.h"
#include "grafoPonderadoMatriz.h"
#include "grafoPonderadoLista.h"


void implementacaoMatrizAdj()
{
    
    //Matriz
    char ** matrizAdj;
    VerticeMatriz * listaVertices;
    int qtdVertices = 0, indexRaiz, indexRaiz2, i , j, verticePartida, qtdVerticesBusca = 0;
    int minutos, segundos;
    No * noRaiz;
    No * inicioFila;
    
    char * nomeArquivoEntrada [150];
    char * nomeArquivoSaida = "./arquivosDeSaida/saida.txt";
    struct timeval inicio, final;
    float tBuscaProfundidade, tBuscaLargura, tPartesConexas, tLeitura, tCaminho;
    printf("\n\nInforme o caminho do arquivo de entrada:\n");
    getchar();
    gets( nomeArquivoEntrada);
    
    printf("Iniciando array de vertices e matriz de adjacência Aguarde... \n");
    //Lendo quantidade de Vértices e inicializando vetor e matriz
    lerQuantidadeVertices(nomeArquivoEntrada, &qtdVertices);
    
    matrizAdj = (char **)calloc(qtdVertices, sizeof(char *));
        
    for(i = 0; i < qtdVertices; i++)
    {
        matrizAdj[i] = (char *)calloc(qtdVertices, sizeof(char *));
    }
    
    for(i = 0; i < qtdVertices; i++)
    {
        for(j = 0; j < qtdVertices; j++)
        {
            matrizAdj[i][j] = '0';
        }
    }
    
    listaVertices = (VerticeMatriz *)calloc(qtdVertices, sizeof(VerticeMatriz));
    
    for(i = 0; i < qtdVertices; i++)
    {
        listaVertices[i].id = -1;
           
    }
    printf("Lendo o grafo Aguarde... \n");
    gettimeofday(&inicio, NULL);
    lerGrafoMatriz(nomeArquivoEntrada, nomeArquivoSaida, matrizAdj, listaVertices);
    gettimeofday(&final, NULL);
    tLeitura =  (final.tv_sec - inicio.tv_sec) ;
    minutos = tLeitura/60;
    segundos = ((int)tLeitura)%60;
    printf("O tempo de leitura do arquivo foi %dm:%ds\n", minutos, segundos );
    
    //--------------------------------------------------------------------
    
    int comando = 0;
    
    
    while(comando!=6)
    {
        printf("\n\nEscolha uma opção");
        printf("\n1 - realizar todas as operações");
        printf("\n2 - criar árvore geradora com busca em profundidade");
        printf("\n3 - criar árvore geradora com busca em Largura");
        printf("\n4 - realizar conta de partes conexas");
        printf("\n5 - calcular caminho entre 2 vértices");
        printf("\n6 - Para voltar no menu\n\n");

        scanf("%d",&comando);
       
        switch(comando)
        {
            case 1:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("realizando operações Aguarde... \n");
                
                noRaiz = criarNo(-1);
                
                gettimeofday(&inicio, NULL);
                qtdVerticesBusca = 0;
                buscaProfundidadeMatriz("./arquivosDeSaida/buscaProfundidadeMatrizSaida.txt", indexRaiz, -1, listaVertices, &qtdVertices, matrizAdj,noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec);
                
                
                free(noRaiz);
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraMatriz("./arquivosDeSaida/buscaLarguraMatrizSaida.txt", indexRaiz, -1,listaVertices, &qtdVertices, matrizAdj, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                
                tBuscaLargura =  (final.tv_sec - inicio.tv_sec);
                
                free(noRaiz);
                noRaiz = criarNo(-1);
                gettimeofday(&inicio, NULL);
                verificarPartesConexasMatriz("./arquivosDeSaida/partesConexasMatriz.txt", listaVertices, &qtdVertices, matrizAdj);
                gettimeofday(&final, NULL);
                
                tPartesConexas = (final.tv_sec - inicio.tv_sec);
                
                
                free(noRaiz);
                free(inicioFila);
                
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
            }
            case 2:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                qtdVerticesBusca = 0;
                
                gettimeofday(&inicio, NULL);
                buscaProfundidadeMatriz("./arquivosDeSaida/buscaProfundidadeMatrizSaida.txt", indexRaiz, -1, listaVertices, &qtdVertices, matrizAdj,noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec) ;
                                
                free(noRaiz);
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );

                break;
            }
            case 3:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraMatriz("./arquivosDeSaida/buscaLarguraMatrizSaida.txt", indexRaiz, -1,listaVertices, &qtdVertices, matrizAdj, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                
                tBuscaLargura = (final.tv_sec - inicio.tv_sec) ;
                
                free(noRaiz);
                free(inicioFila);
                
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                                
                break;
            }
            case 4:
            {
                
                gettimeofday(&inicio, NULL);
                verificarPartesConexasMatriz("./arquivosDeSaida/partesConexasMatriz.txt", listaVertices, &qtdVertices, matrizAdj);
                gettimeofday(&final, NULL);
                
                tPartesConexas =  (final.tv_sec - inicio.tv_sec) ;
                
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
                
            }
            case 5:
            {
                printf("Informe o vértice de Origem: ");
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de Origem: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("Informe o vértice de destino: ");
                getchar();
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz2 = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz2 == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de destino: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz2 = buscarVerticeMatriz(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("realizando operações Aguarde... \n");
                
                
                gettimeofday(&inicio, NULL);
                prepararCalculoCaminhoMatriz(listaVertices, &qtdVertices);
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                buscaLarguraMatriz("./arquivosDeSaida/buscaLarguraMatrizSaida2.txt", indexRaiz, -1,listaVertices, &qtdVertices, matrizAdj, noRaiz, inicioFila);
                free(noRaiz);
                free(inicioFila);
                
                caminhoEntreVerticesMatriz(indexRaiz, indexRaiz2, listaVertices,&qtdVertices);
                
                gettimeofday(&final, NULL);
                
                tCaminho = (final.tv_sec - inicio.tv_sec);
                
                minutos = tCaminho/60;
                segundos = ((int)tCaminho)%60;
                printf("O tempo de execucao do caminho foi %dm:%ds\n", minutos, segundos );
                
                break;
            }
            case 6:
                continue;
                break;
             
            default:
              printf("\n\nNenhuma opcao foi escolhida.");
        }
        getchar();
        
    }
    
    for(i = 0; i < qtdVertices; i++)
    {
        free(matrizAdj[i]);
    }
    
    free(listaVertices);
    free(matrizAdj);
    
    
}

void implementacaoMatrizAdjPonderado()
{
    
    //Matriz
    float ** matrizAdj;
    VerticeMatrizP * listaVertices;
    int qtdVertices = 0, indexRaiz, indexRaiz2, i , j, verticePartida, qtdVerticesBusca = 0;
    int minutos, segundos;
    NoP * noRaiz;
    NoP * inicioFila;
    
    char * nomeArquivoEntrada [150];
    char * nomeArquivoSaida = "./arquivosDeSaida/saidaMatrizPonderada.txt";
    struct timeval inicio, final;
    float tBuscaProfundidade, tBuscaLargura, tPartesConexas, tLeitura, tDijkstra;
    printf("\n\nInforme o caminho do arquivo de entrada:\n");
    getchar();
    gets( nomeArquivoEntrada);
    
    printf("Iniciando array de vertices e matriz de adjacência Aguarde... \n");
    //Lendo quantidade de Vértices e inicializando vetor e matriz
    lerQuantidadeVertices(nomeArquivoEntrada, &qtdVertices);
    
    matrizAdj = (float **)calloc(qtdVertices, sizeof(float *));
        
    for(i = 0; i < qtdVertices; i++)
    {
        matrizAdj[i] = (float *)calloc(qtdVertices, sizeof(float*));
    }
    
    for(i = 0; i < qtdVertices; i++)
    {
        for(j = 0; j < qtdVertices; j++)
        {
            matrizAdj[i][j] = -1.0;
        }
    }
    
    listaVertices = (VerticeMatrizP *)calloc(qtdVertices, sizeof(VerticeMatrizP));
    
    for(i = 0; i < qtdVertices; i++)
    {
        listaVertices[i].id = -1;
        
    }
    
    printf("Lendo o grafo Aguarde... \n");
    gettimeofday(&inicio, NULL);
    int result = lerGrafoMatrizP(nomeArquivoEntrada, nomeArquivoSaida, matrizAdj, listaVertices, &qtdVertices);
    gettimeofday(&final, NULL);
    tLeitura =  (final.tv_sec - inicio.tv_sec) ;
    minutos = tLeitura/60;
    segundos = ((int)tLeitura)%60;
    printf("O tempo de leitura do arquivo foi %dm:%ds\n", minutos, segundos );
    if(result == 1){
        
        //--------------------------------------------------------------------
        
        int comando = 0;
        
        
        while(comando!=6)
        {
            printf("\n\nEscolha uma opção");
            printf("\n1 - realizar todas as operações");
            printf("\n2 - criar árvore geradora com busca em profundidade");
            printf("\n3 - criar árvore geradora com busca em Largura");
            printf("\n4 - realizar conta de partes conexas");
            printf("\n5 - encontrar caminho com  algoritmo de dijkstra");
            printf("\n6 - Para voltar no menu\n\n");
            //fflush(stdin);
            scanf("%d",&comando);
        
            switch(comando)
            {
                case 1:
                {
                    printf("Informe o vértice de Origem: ");
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de Origem: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("Informe o vértice de destino: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz2 = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz2 == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de destino: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz2 = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("realizando operações Aguarde... \n");
                    
                    gettimeofday(&inicio, NULL);
                    caminhoDijkstraMatriz(indexRaiz, indexRaiz2, matrizAdj, listaVertices, &qtdVertices);
                    gettimeofday(&final, NULL);
                    
                    tDijkstra = (final.tv_sec - inicio.tv_sec);
                    
                    noRaiz = criarNoP(-1, -1);
                    
                    gettimeofday(&inicio, NULL);
                    qtdVerticesBusca = 0;
                    buscaProfundidadeMatrizP("./arquivosDeSaida/buscaProfundidadeMatrizPonderadaSaida.txt", indexRaiz, -1, listaVertices, &qtdVertices, matrizAdj,noRaiz, 0, &qtdVerticesBusca);
                    gettimeofday(&final, NULL);
                    
                    tBuscaProfundidade = (final.tv_sec - inicio.tv_sec);
                    
                    
                    free(noRaiz);
                    noRaiz = criarNoP(-1, -1);
                    inicioFila = criarNoP(-1, -1);
                    gettimeofday(&inicio, NULL);
                    buscaLarguraMatrizP("./arquivosDeSaida/buscaLarguraMatrizPonderadaSaida.txt", indexRaiz, -1,listaVertices, &qtdVertices, matrizAdj, noRaiz, inicioFila);
                    gettimeofday(&final, NULL);
                    
                    tBuscaLargura =  (final.tv_sec - inicio.tv_sec);
                    
                    free(noRaiz);
                    noRaiz = criarNoP(-1, -1);
                    gettimeofday(&inicio, NULL);
                    verificarPartesConexasMatrizP("./arquivosDeSaida/partesConexasMatrizPonderada.txt", listaVertices, &qtdVertices, matrizAdj);
                    gettimeofday(&final, NULL);
                    
                    tPartesConexas = (final.tv_sec - inicio.tv_sec);
                    
                    
                    free(noRaiz);
                    free(inicioFila);
                    
                    minutos = tBuscaProfundidade/60;
                    segundos = ((int)tBuscaProfundidade)%60;
                    printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );
                    minutos = tBuscaLargura/60;
                    segundos = ((int)tBuscaLargura)%60;
                    printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                    minutos = tPartesConexas/60;
                    segundos = ((int)tPartesConexas)%60;
                    printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                    minutos = tDijkstra/60;
                    segundos = ((int)tDijkstra)%60;
                    printf("O tempo de execucao do algoritmo de Dijkstra foi %dm:%ds\n", minutos, segundos );
                    
                    break;
                }
                case 2:
                {
                    printf("Informe o vértice de partida: ");
                    scanf("%d",&verticePartida);
                    
                    indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de partida: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    noRaiz = criarNoP(-1, -1);
                    qtdVerticesBusca = 0;
                    
                    gettimeofday(&inicio, NULL);
                    buscaProfundidadeMatrizP("./arquivosDeSaida/buscaProfundidadeSaida.txt", indexRaiz, -1, listaVertices, &qtdVertices, matrizAdj,noRaiz, 0, &qtdVerticesBusca);
                    gettimeofday(&final, NULL);
                    
                    tBuscaProfundidade = (final.tv_sec - inicio.tv_sec) ;
                                    
                    free(noRaiz);
                    minutos = tBuscaProfundidade/60;
                    segundos = ((int)tBuscaProfundidade)%60;
                    printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );

                    break;
                }
                case 3:
                {
                    printf("Informe o vértice de partida: ");
                    scanf("%d",&verticePartida);
                    
                    indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de partida: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    noRaiz = criarNoP(-1, -1);
                    inicioFila = criarNoP(-1, -1);
                    gettimeofday(&inicio, NULL);
                    buscaLarguraMatrizP("./arquivosDeSaida/buscaLarguraMatrizPonderadaSaida.txt", indexRaiz, -1,listaVertices, &qtdVertices, matrizAdj, noRaiz, inicioFila);
                    gettimeofday(&final, NULL);
                    
                    tBuscaLargura = (final.tv_sec - inicio.tv_sec) ;
                    
                    free(noRaiz);
                    free(inicioFila);
                    
                    minutos = tBuscaLargura/60;
                    segundos = ((int)tBuscaLargura)%60;
                    printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                                    
                    break;
                }
                case 4:
                {
                    
                    gettimeofday(&inicio, NULL);
                    verificarPartesConexasMatrizP("./arquivosDeSaida/partesConexasMatrizPonderada.txt", listaVertices, &qtdVertices, matrizAdj);
                    gettimeofday(&final, NULL);
                    
                    tPartesConexas =  (final.tv_sec - inicio.tv_sec) ;
                    
                    minutos = tPartesConexas/60;
                    segundos = ((int)tPartesConexas)%60;
                    printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                    
                    break;
                    
                }
                case 5:
                {
                    printf("Informe o vértice de Origem: ");
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de Origem: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("Informe o vértice de destino: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz2 = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz2 == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de destino: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz2 = buscarVerticeMatrizP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("realizando operações Aguarde... \n");
                    
                    
                    gettimeofday(&inicio, NULL);
                    caminhoDijkstraMatriz(indexRaiz, indexRaiz2, matrizAdj, listaVertices, &qtdVertices);
                    gettimeofday(&final, NULL);
                    
                    tDijkstra = (final.tv_sec - inicio.tv_sec);
                    
                    minutos = tDijkstra/60;
                    segundos = ((int)tDijkstra)%60;
                    printf("O tempo de execucao do algoritmo de Dijkstra foi %dm:%ds\n", minutos, segundos );
                    
                    break;
                }
                case 6:
                    continue;
                    break;
                default:
                printf("\n\nNenhuma opcao foi escolhida.");
            }
            getchar();
            
        }
        
        for(i = 0; i < qtdVertices; i++)
        {
            free(matrizAdj[i]);
        }
        
        free(listaVertices);
        free(matrizAdj);
    }else{
        printf("\n\nGrafo com peso negativo nas arestas.\n\n");
    }
    
    
}

void implementacaoListaAdj()
{
    
    //Lista
    VerticeLista* listaVertices;
    int qtdVertices = 0, indexRaiz, indexRaiz2, i, comando = 0, verticePartida, qtdVerticesBusca = 0;
    No* aux;
    No * noRaiz  = criarNo(-1);
    No * inicioFila  = criarNo(-1);
    char * nomeArquivoEntrada [150];
    char * nomeArquivoSaida = "./arquivosDeSaida/saidaLista.txt";
    struct timeval inicio, final;
    float tBuscaProfundidade, tBuscaLargura, tPartesConexas, tLeitura, tCaminho;
    int minutos, segundos;
    
    printf("\n\nInforme o caminho do arquivo de entrada:\n");
    fflush(stdin);
    getchar();
    gets(nomeArquivoEntrada);
    
    printf("Iniciandi array de vertices Aguarde... \n");
    lerQuantidadeVertices(nomeArquivoEntrada, &qtdVertices);
    
    listaVertices = (VerticeLista *)calloc(qtdVertices, sizeof(VerticeLista));
    
    
    for(i = 0; i < qtdVertices; i++)
    {
        
        listaVertices[i].id = -1;
        listaVertices[i].grau = 0;
        listaVertices[i].nivel = 0;
        aux = criarNo(-1);
        listaVertices[i].aresta = aux;
           
    }
    
    printf("Lendo o grafo Aguarde... \n");
    gettimeofday(&inicio, NULL);
    lerGrafoLista(nomeArquivoEntrada, nomeArquivoSaida,listaVertices);
    gettimeofday(&final, NULL);
    tLeitura =  (final.tv_sec - inicio.tv_sec) ;
    minutos = tLeitura/60;
    segundos = ((int)tLeitura)%60;
    printf("O tempo de tLeitura do arquivo foi %dm:%ds\n", minutos, segundos );
    
    
    //------------------------------------------------------------------------
    
    while(comando!=6)
    {
        printf("\n\nEscolha uma opção");
        printf("\n1 - realizar todas as operações");
        printf("\n2 - criar árvore geradora com busca em profundidade");
        printf("\n3 - criar árvore geradora com busca em Largura");
        printf("\n4 - realizar conta de partes conexas");
        printf("\n5 - calcular caminho entre 2 vértices");
        printf("\n6 - Para voltar no menu\n\n");
        //fflush(stdin);
        scanf("%d",&comando);
       
        switch(comando)
        {
            case 1:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("realizando operações Aguarde... \n");
                
                noRaiz = criarNo(-1);
                qtdVerticesBusca = 0;
                
                gettimeofday(&inicio, NULL);
                buscaProfundidadeLista("./arquivosDeSaida/buscaProfundidadeSaidaLista.txt", indexRaiz,-1, listaVertices, &qtdVertices, noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec);
                
                
                free(noRaiz);
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraLista("./arquivosDeSaida/buscaLarguraSaidaLista.txt", indexRaiz, -1,listaVertices, &qtdVertices, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                free(noRaiz);
                free(inicioFila);
                
                tBuscaLargura =  (final.tv_sec - inicio.tv_sec);
                
                gettimeofday(&inicio, NULL);
                verificarPartesConexasLista("./arquivosDeSaida/partesConexasLista.txt", listaVertices, &qtdVertices);
                gettimeofday(&final, NULL);
                
                tPartesConexas = (final.tv_sec - inicio.tv_sec);
                
                
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
            }
            case 2:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                qtdVerticesBusca = 0;
                gettimeofday(&inicio, NULL);
                buscaProfundidadeLista("./arquivosDeSaida/buscaProfundidadeSaidaLista.txt", indexRaiz,-1, listaVertices, &qtdVertices, noRaiz, 0, &qtdVerticesBusca);
                gettimeofday(&final, NULL);
                
                tBuscaProfundidade = (final.tv_sec - inicio.tv_sec) ;
                                
                free(noRaiz);
                minutos = tBuscaProfundidade/60;
                segundos = ((int)tBuscaProfundidade)%60;
                printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );

                break;
            }
            case 3:
            {
                printf("Informe o vértice de partida: ");
                scanf("%d",&verticePartida);
                
                indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de partida: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                gettimeofday(&inicio, NULL);
                buscaLarguraLista("./arquivosDeSaida/buscaLarguraSaidaLista.txt", indexRaiz, -1,listaVertices, &qtdVertices, noRaiz, inicioFila);
                gettimeofday(&final, NULL);
                
                tBuscaLargura = (final.tv_sec - inicio.tv_sec) ;
                
                free(noRaiz);
                free(inicioFila);
                
                minutos = tBuscaLargura/60;
                segundos = ((int)tBuscaLargura)%60;
                printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                                
                break;
            }
            case 4:
            {
                
                gettimeofday(&inicio, NULL);
                verificarPartesConexasLista("./arquivosDeSaida/partesConexasLista.txt", listaVertices, &qtdVertices);
                gettimeofday(&final, NULL);
                
                tPartesConexas =  (final.tv_sec - inicio.tv_sec) ;
                
                minutos = tPartesConexas/60;
                segundos = ((int)tPartesConexas)%60;
                printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                
                break;
                
            }
            case 5:
            {
                printf("Informe o vértice de Origem: ");
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de Origem: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("Informe o vértice de destino: ");
                getchar();
                scanf("%d",&verticePartida);
                printf("Buscando vértice no grafo Aguarde... \n");
                indexRaiz2 = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                
                while(indexRaiz2 == -1)
                {
                    printf("Vértice não encontrado\nInforme o vértice de destino: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    indexRaiz2 = buscarVerticeLista(listaVertices, verticePartida, &qtdVertices);
                }
                
                printf("realizando operações Aguarde... \n");
                
                
                gettimeofday(&inicio, NULL);
                prepararCalculoCaminhoLista(listaVertices, &qtdVertices);
                noRaiz = criarNo(-1);
                inicioFila = criarNo(-1);
                buscaLarguraLista("./arquivosDeSaida/buscaLarguraSaidaLista2.txt", indexRaiz, -1,listaVertices, &qtdVertices, noRaiz, inicioFila);
                free(noRaiz);
                free(inicioFila);
                
                caminhoEntreVerticesLista(indexRaiz, indexRaiz2, listaVertices,&qtdVertices);
                
                gettimeofday(&final, NULL);
                
                tCaminho = (final.tv_sec - inicio.tv_sec);
                
                minutos = tCaminho/60;
                segundos = ((int)tCaminho)%60;
                printf("O tempo de execucao do caminho foi %dm:%ds\n", minutos, segundos );
                
                break;
            }
            case 6:
                continue;
                break;
             
            default:
              printf("\n\nNenhuma opcao foi escolhida.");
        }
        getchar();
        
    }
    free(listaVertices);
    
}

void implementacaoListaAdjPonderado()
{
    
    //Lista
    VerticeListaP* listaVertices;
    int qtdVertices = 0, indexRaiz, indexRaiz2, i, comando = 0, verticePartida, qtdVerticesBusca = 0;
    NoP* aux;
    NoP * noRaiz  = criarNoP(-1, -1);
    NoP * inicioFila  = criarNoP(-1, -1);
    char * nomeArquivoEntrada [150];
    char * nomeArquivoSaida = "./arquivosDeSaida/saidaListaPonderada.txt";
    struct timeval inicio, final;
    float tBuscaProfundidade, tBuscaLargura, tPartesConexas, tLeitura, tDijkstra;
    int minutos, segundos;
    
    printf("\n\nInforme o caminho do arquivo de entrada:\n");
    fflush(stdin);
    getchar();
    gets(nomeArquivoEntrada);
    
    printf("Iniciandi array de vertices Aguarde... \n");
    lerQuantidadeVertices(nomeArquivoEntrada, &qtdVertices);
    
    listaVertices = (VerticeListaP *)calloc(qtdVertices, sizeof(VerticeListaP));
    
    
    for(i = 0; i < qtdVertices; i++)
    {
        
        listaVertices[i].id = -1;
        listaVertices[i].grau = 0;
        listaVertices[i].nivel = 0;
        aux = criarNoP(-1, -1);
        listaVertices[i].aresta = aux;
           
    }
    
    printf("Lendo o grafo Aguarde... \n");
    gettimeofday(&inicio, NULL);
    int result = lerGrafoListaP(nomeArquivoEntrada, nomeArquivoSaida,listaVertices);
    gettimeofday(&final, NULL);
    tLeitura =  (final.tv_sec - inicio.tv_sec) ;
    minutos = tLeitura/60;
    segundos = ((int)tLeitura)%60;
    printf("O tempo de tLeitura do arquivo foi %dm:%ds\n", minutos, segundos );
    
    if(result == 1){
        //imprimirVerticesListaP(listaVertices, &qtdVertices);
        //------------------------------------------------------------------------
        
        while(comando!=6)
        {            
            printf("\n\nEscolha uma opção");
            printf("\n1 - realizar todas as operações");
            printf("\n2 - criar árvore geradora com busca em profundidade");
            printf("\n3 - criar árvore geradora com busca em Largura");
            printf("\n4 - realizar conta de partes conexas");
            printf("\n5 - encontrar caminho com  algoritmo de dijkstra");
            printf("\n6 - Para voltar no menu\n\n");
            fflush(stdin);
            scanf("%d",&comando);
        
            switch(comando)
            {
                case 1:
                {
                    printf("Informe o vértice de Origem: ");
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de Origem: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("Informe o vértice de destino: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz2 = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz2 == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de destino: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz2 = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("realizando operações Aguarde... \n");
                    
                    gettimeofday(&inicio, NULL);
                    caminhoDijkstraLista(indexRaiz, indexRaiz2, listaVertices, &qtdVertices);
                    gettimeofday(&final, NULL);
                    
                    tDijkstra = (final.tv_sec - inicio.tv_sec);
                    
                    
                    noRaiz = criarNoP(-1, -1);
                    qtdVerticesBusca = 0;
                    
                    gettimeofday(&inicio, NULL);
                    buscaProfundidadeListaP("./arquivosDeSaida/buscaProfundidadeSaidaListaPonderada.txt", indexRaiz,-1, listaVertices, &qtdVertices, noRaiz, 0, &qtdVerticesBusca);
                    gettimeofday(&final, NULL);
                    
                    tBuscaProfundidade = (final.tv_sec - inicio.tv_sec);
                    
                    
                    free(noRaiz);
                    noRaiz = criarNoP(-1, -1);
                    inicioFila = criarNoP(-1, -1);
                    gettimeofday(&inicio, NULL);
                    buscaLarguraListaP("./arquivosDeSaida/buscaLarguraSaidaListaPonderada.txt", indexRaiz, -1,listaVertices, &qtdVertices, noRaiz, inicioFila);
                    gettimeofday(&final, NULL);
                    free(noRaiz);
                    free(inicioFila);
                    
                    tBuscaLargura =  (final.tv_sec - inicio.tv_sec);
                    
                    gettimeofday(&inicio, NULL);
                    verificarPartesConexasListaP("./arquivosDeSaida/partesConexasListaPonderada.txt", listaVertices, &qtdVertices);
                    gettimeofday(&final, NULL);
                    
                    tPartesConexas = (final.tv_sec - inicio.tv_sec);
                    
                    
                    minutos = tBuscaProfundidade/60;
                    segundos = ((int)tBuscaProfundidade)%60;
                    printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );
                    minutos = tBuscaLargura/60;
                    segundos = ((int)tBuscaLargura)%60;
                    printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                    minutos = tPartesConexas/60;
                    segundos = ((int)tPartesConexas)%60;
                    printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                    minutos = tDijkstra/60;
                    segundos = ((int)tDijkstra)%60;
                    printf("O tempo de execucao do algoritmo de Dijkstra foi %dm:%ds\n", minutos, segundos );
                    
                    break;
                }
                case 2:
                {
                    printf("Informe o vértice de partida: ");
                    scanf("%d",&verticePartida);
                    
                    indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de partida: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    noRaiz = criarNoP(-1, -1);
                    qtdVerticesBusca = 0;
                    gettimeofday(&inicio, NULL);
                    buscaProfundidadeListaP("./arquivosDeSaida/buscaProfundidadeSaidaListaPonderada.txt", indexRaiz,-1, listaVertices, &qtdVertices, noRaiz, 0, &qtdVerticesBusca);
                    gettimeofday(&final, NULL);
                    
                    tBuscaProfundidade = (final.tv_sec - inicio.tv_sec) ;
                                    
                    free(noRaiz);
                    minutos = tBuscaProfundidade/60;
                    segundos = ((int)tBuscaProfundidade)%60;
                    printf("O tempo de execucao da busca em profundidade foi %dm:%ds\n", minutos, segundos );

                    break;
                }
                case 3:
                {
                    printf("Informe o vértice de partida: ");
                    scanf("%d",&verticePartida);
                    
                    indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de partida: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    noRaiz = criarNoP(-1, -1);
                    inicioFila = criarNoP(-1, -1);
                    gettimeofday(&inicio, NULL);
                    buscaLarguraListaP("./arquivosDeSaida/buscaLarguraSaidaListaPonderada.txt", indexRaiz, -1,listaVertices, &qtdVertices, noRaiz, inicioFila);
                    gettimeofday(&final, NULL);
                    
                    tBuscaLargura = (final.tv_sec - inicio.tv_sec) ;
                    
                    free(noRaiz);
                    free(inicioFila);
                    
                    minutos = tBuscaLargura/60;
                    segundos = ((int)tBuscaLargura)%60;
                    printf("O tempo de execucao da busca em Largura foi %dm:%ds\n", minutos, segundos );
                                    
                    break;
                }
                case 4:
                {
                    
                    gettimeofday(&inicio, NULL);
                    verificarPartesConexasListaP("./arquivosDeSaida/partesConexasListaPonderada.txt", listaVertices, &qtdVertices);
                    gettimeofday(&final, NULL);
                    
                    tPartesConexas =  (final.tv_sec - inicio.tv_sec) ;
                    
                    minutos = tPartesConexas/60;
                    segundos = ((int)tPartesConexas)%60;
                    printf("O tempo de execucao das partes conexas foi %dm:%ds\n", minutos, segundos );
                    
                    break;
                    
                }
                case 5:
                {
                    printf("Informe o vértice de Origem: ");
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de Origem: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("Informe o vértice de destino: ");
                    getchar();
                    scanf("%d",&verticePartida);
                    printf("Buscando vértice no grafo Aguarde... \n");
                    indexRaiz2 = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    
                    while(indexRaiz2 == -1)
                    {
                        printf("Vértice não encontrado\nInforme o vértice de destino: ");
                        getchar();
                        scanf("%d",&verticePartida);
                        indexRaiz2 = buscarVerticeListaP(listaVertices, verticePartida, &qtdVertices);
                    }
                    
                    printf("realizando operações Aguarde... \n");
                    
                    gettimeofday(&inicio, NULL);
                    caminhoDijkstraLista(indexRaiz, indexRaiz2, listaVertices, &qtdVertices);
                    gettimeofday(&final, NULL);
                    
                    tDijkstra = (final.tv_sec - inicio.tv_sec);
                    
                    minutos = tDijkstra/60;
                    segundos = ((int)tDijkstra)%60;
                    printf("O tempo de execucao do algoritmo de Dijkstra foi %dm:%ds\n", minutos, segundos );
                
                    break;
                }
                case 6:
                    continue;
                    break;
                default:
                printf("\n\nNenhuma opcao foi escolhida.");
            }
            getchar();
            
        }
        free(listaVertices);
    
    }else{
        printf("\n\nGrafo com peso negativo nas arestas.\n\n");
    }
    
}

int main(int argc, char ** argv)
{
    int comando = 0;
    
    while(comando!=3)
    {
        printf("\n\nEscolha o tipo de representação");
        printf("\n1 - Matriz de adjacência");
        printf("\n2 - Lista de adjacência");
        printf("\n3 - Para Fechar o programa\n\n");
        fflush(stdin);
        
        scanf("%d",&comando);
       
        switch(comando)
        {
            case 1:
                while(comando!=3)
                {
                    printf("\n\nEscolha a classificação do grafo");
                    printf("\n1 - Grafo simples, não dirigido");
                    printf("\n2 - Grafo simples Ponderado, não dirigido");
                    printf("\n3 - Para voltar no menu\n\n");
                    fflush(stdin);
                    
                    scanf("%d",&comando);
                
                    switch(comando)
                    {
                        case 1:
                            implementacaoMatrizAdj();
                            getchar();
                            break;
                        case 2:
                            implementacaoMatrizAdjPonderado();
                            getchar();
                            break;
                        case 3:
                            break;
                        
                        default:
                        printf("\n\nNenhuma opcao foi escolhida.");
                        getchar();
                    }
                }
                
                comando = 0;
                break;
            case 2:
                while(comando!=3)
                {
                    printf("\n\nEscolha a classificação do grafo");
                    printf("\n1 - Grafo simples, não dirigido");
                    printf("\n2 - Grafo simples Ponderado, não dirigido");
                    printf("\n3 - Para voltar no menu\n\n");
                    fflush(stdin);
                    
                    scanf("%d",&comando);
                
                    switch(comando)
                    {
                        case 1:
                            implementacaoListaAdj();
                            getchar();
                            break;
                        case 2:
                            implementacaoListaAdjPonderado();
                            getchar();
                            break;
                        case 3:
                            break;
                        
                        default:
                        printf("\n\nNenhuma opcao foi escolhida.");
                        getchar();
                    }    
                }
                
                comando = 0;
                break;
            case 3:
                continue;
                break;
             
            default:
              printf("\n\nNenhuma opcao foi escolhida.");
        }
        
        getchar();
        
        
    }
    
    return 0;
}
