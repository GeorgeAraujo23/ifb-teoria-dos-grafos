/**
 * @brief "Esse código implementa uma biblioteca para grafos, ponderados e não dirigidos, usando Matriz de adjacência"
 * @file grafoPonderadoMatriz.c
 * @author George Araújo - george.ifrn@gmail.com
 * @since 22/11/2016
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include "listaPonderada.h"
#include "grafoPonderadoMatriz.h"


/**Função imprime identificador e index de todos os Vértices do paramListaVertices
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return
 */
void imprimirVerticesMatrizP(VerticeMatrizP * paramListaVertices, int * paramMaxIndice)
{
    
    for(int i = 0; i <= *paramMaxIndice; i++)
    {
        printf("%d %d \n", paramListaVertices[i].id, i);
    }
}

/**Função imprime a matriz passada
 * @param paramMatrizAdj uma referência para um vertor de VerticeMatrizP
 * @param paramQtdVertices um inteiro que indica o tamanho da matriz
 * @sa 
 * @return
 */
void imprimirMatrizAdjacenciaP(float ** paramMatrizAdj, int * paramQtdVertices)
{
    
    for(int i = 0; i < *paramQtdVertices; i++)
    {
        for(int j = 0; j < *paramQtdVertices; j++)
        {
            printf("%f ", paramMatrizAdj[i][j]);
        }
        printf("\n ");
    }
    
}

/**Função busca um VerticeMatrizP no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP
 * @param paramVertice um inteiro com o identificador do VerticeMatrizP procurado
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com valor -1 caso não tenha encontrado o VerticeMatriz ou  o index do vetor na posição onde o VerticeMatrizP se encontra
 */
int buscarVerticeMatrizP(VerticeMatrizP * paramListaVertices, int paramVertice, int *paramMaxIndice)
{
    
    for(int i = 0; i <= *paramMaxIndice; i++)
    {
        if(paramListaVertices[i].id == paramVertice)
        {
            return i;
            
        }
    }
    
    return -1;
    
}


/**Função insere um VerticeMatrizP no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP
 * @param paramVertice um inteiro com o identificador do novo VerticeMatrizP
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com o novo tamanho usado do vetor paramListaVertices
 */
int inserirVerticeMatrizP(VerticeMatrizP * paramListaVertices, int paramVertice, int * paramMaxIndice)
{
    
    *paramMaxIndice = *paramMaxIndice + 1;
    
    paramListaVertices[*paramMaxIndice].id = paramVertice;
    paramListaVertices[*paramMaxIndice].grau = 0;
    paramListaVertices[*paramMaxIndice].nivel = 0;
    paramListaVertices[*paramMaxIndice].fechado = 'N';
    paramListaVertices[*paramMaxIndice].peso = -1.0;
    paramListaVertices[*paramMaxIndice].indexAntecessor = -1;
    return *paramMaxIndice;
}

/**Função faz uma busca em largura no grafo a partir de um VerticeMatrizP
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeMatrizP que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeMatrizP pai
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeMatrizP já visitados
 * @param paramInicioFila um No cabeça para uma fila que indica ordem dos VerticeMatrizP que devem ser visitados
 * @sa criarNoP inserirInicioListaP inserirFinalListaP removerNoInicioP pesquisarNoP
 * @return
 */
void buscaLarguraMatrizP(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeMatrizP * paramListaVertices, int *paramQtdVertices, float ** paramMatrizAdj,NoP * paramNosVisitados, NoP * paramInicioFila)
{
    NoP *noVisitado;
    NoP *noAuxFila;
    NoP *novoNoFila;
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaLarguraMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {
        if(paramIndexPai == -1)
        {
            paramListaVertices[paramIndexRaiz].nivel = 0;
            noVisitado = criarNoP(paramIndexRaiz, -1);
            inserirInicioListaP(paramNosVisitados, noVisitado);
            
            fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, 0, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        }
        
        for(int i = 0; i < *paramQtdVertices; i++)
        {
            if(paramMatrizAdj[paramIndexRaiz][i] != 0)
            {    
                if(pesquisarNoP(paramNosVisitados, i) == NULL)
                {    
                    noVisitado = criarNoP(i, -1);
                    inserirInicioListaP(paramNosVisitados, noVisitado);

                    novoNoFila = criarNoP(i, -1);
                    inserirFinalListaP(paramInicioFila, novoNoFila);
                    
                    paramListaVertices[i].nivel = paramListaVertices[paramIndexRaiz].nivel + 1;
                    
                    fprintf(arquivoSaida, "%d %d %d \n", paramListaVertices[i].id,paramListaVertices[i].nivel, paramListaVertices[paramIndexRaiz].id);
                }
                    
            }
        }
        
        fclose(arquivoSaida);
        
        if(paramInicioFila->prox != NULL)
        {   
            noAuxFila = removerNoInicioP(paramInicioFila);
            buscaLarguraMatrizP(paramNomeArquivoSaida, noAuxFila->id, paramIndexRaiz,paramListaVertices, paramQtdVertices, paramMatrizAdj, paramNosVisitados, paramInicioFila);  
            
        }
    }
    
}

/**Função faz uma busca em profundidade no grafo a partir de um VerticeMatrizP
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeMatrizP que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeMatrizP pai
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeMatrizP já visitados
 * @param paramNivel um inteiro que indica o nivel do VerticeMatrizP pai
 * @sa criarNoP inserirInicioListaP pesquisarNoP
 * @return
 */
void buscaProfundidadeMatrizP(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeMatrizP * paramListaVertices, int* paramQtdVertices, float ** paramMatrizAdj, NoP * paramNosVisitados, int paramNivel, int* paramQtdVerticesBusca )
{
    NoP *noVisitado = criarNoP(paramIndexRaiz, -1);
    inserirInicioListaP(paramNosVisitados, noVisitado);
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaProfundidadeMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, paramNivel, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        *paramQtdVerticesBusca = *paramQtdVerticesBusca + 1;
        fclose(arquivoSaida);
        paramNivel++;
        
        for(int i = 0; i < *paramQtdVertices; i++)
        {
            if(paramMatrizAdj[paramIndexRaiz][i] != 0)
            {    
                if(pesquisarNoP(paramNosVisitados, i) == NULL){
                    
                    buscaProfundidadeMatrizP(paramNomeArquivoSaida, i,paramIndexRaiz, paramListaVertices, paramQtdVertices, paramMatrizAdj, paramNosVisitados, paramNivel, paramQtdVerticesBusca);
                }
                    
            }
        }
    }
}

/**Função lê o arquivo de entrada com o grafo e gera o vetor de VerticeMatrizP e a matriz de adjacência
 * @param paramNomeArquivoEntrada nome do arquivo de entrada do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida do grafo
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP onde seŕão armazenados os vértices
 * @sa buscarVerticeMatrizP inserirVerticeMatrizP
 * @return inteiro indicando se houve erro na função, retorna 1 se a leitura for feita com sucesso e 0 se algum peso do grafo for menor que zero
 */
int lerGrafoMatrizP(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,float ** paramMatrizAdj, VerticeMatrizP * paramListaVertices, int* paramQtdVertices)
{
    int resultado = 1;
    FILE * entrada;
    FILE * saida;
    float peso;
    int vertice1, vertice2,indexVertice1, indexVertice2, maxIndice = -1, qtdVertices = 0, qtdArestas = 0;
    
    entrada = fopen(paramNomeArquivoEntrada, "r");
    if(entrada == NULL)
    {
        printf("Método: lerGrafoMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoEntrada);
        exit(0);
    }else
    {
        fscanf(entrada, "%d",&qtdVertices);
        
        while(!feof(entrada))
        {
            fscanf(entrada, "%d %d %f", &vertice1, &vertice2, &peso);
            
            indexVertice1 = buscarVerticeMatrizP(paramListaVertices, vertice1, &maxIndice);
            if(indexVertice1 == -1)
            {
                indexVertice1 = inserirVerticeMatrizP(paramListaVertices, vertice1, &maxIndice);
            }
            
            indexVertice2 = buscarVerticeMatrizP(paramListaVertices, vertice2, &maxIndice);
            if(indexVertice2 == -1)
            {
                indexVertice2 = inserirVerticeMatrizP(paramListaVertices, vertice2, &maxIndice);
            }
            
            paramMatrizAdj[indexVertice1][indexVertice2] = peso;
            paramMatrizAdj[indexVertice2][indexVertice1] = peso;
            
            qtdArestas++;
            paramListaVertices[indexVertice2].grau++;
            paramListaVertices[indexVertice1].grau++;
            if(peso < 0.0)
            {
                resultado = 0;
            }
            
        }
                 
        fclose(entrada);
        qtdArestas--;
	maxIndice++;
        
        saida = fopen(paramNomeArquivoSaida, "w");
        fprintf(saida, "N = %d\n", maxIndice + 1);
        fprintf(saida, "M = %d\n", qtdArestas);
        for(int i = 0; i < maxIndice; i++)
        {
            fprintf(saida, "%d %d\n", paramListaVertices[i].id, paramListaVertices[i].grau);                    
        }
        
        fclose(saida); 
    }
    
    return resultado;
}

/**Função implementa o algoritmo de Dijkstra para caminho mínimo
 * @param paramIndexOrigem index do vértice de partida do caminho
 * @param paramIndexDestino index do vértice de chegada do caminho
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP onde seŕão armazenados os vérticesparamQtdVertices
 * @param paramQtdVertices indica a quantidade de vértices do grafo
 * @sa inserirInicioListaOrdenadaP removerNoInicioP criarNoP pesquisarNoP removerNoP inserirInicioListaP
 * @return
 */
void caminhoDijkstraMatriz(int paramIndexOrigem, int paramIndexDestino,float ** paramMatrizAdj, VerticeMatrizP * paramListaVertices, int* paramQtdVertices)
{
    for(int h = 0; h < *paramQtdVertices; h++)
    {
        paramListaVertices[h].fechado = 'N';
        paramListaVertices[h].peso = -1.0;
        paramListaVertices[h].indexAntecessor = -1;
    }
    FILE * arquivoSaida;
    paramListaVertices[paramIndexOrigem].fechado = 'N';
    paramListaVertices[paramIndexOrigem].peso = 0.0;
    paramListaVertices[paramIndexOrigem].indexAntecessor = -1.0;
    NoP * NoPesquisa;
    NoP * NoRaiz = criarNoP(-1, -1);
    NoP* novoNo  = criarNoP(paramIndexOrigem, 0.0);
    inserirInicioListaOrdenadaP(NoRaiz, novoNo);
    
    while(NoRaiz->prox != NULL)
    {
        int j = NoRaiz->prox->id;
        float peso = NoRaiz->prox->peso;
        removerNoInicioP(NoRaiz);
        paramListaVertices[j].fechado = 'S';
        
        for(int i = 0; i < *paramQtdVertices; i++)
        {
            if(paramMatrizAdj[j][i] != -1)
            {
                if(paramListaVertices[i].fechado == 'N'){
                    
                    if(paramListaVertices[i].peso == -1 || paramListaVertices[i].peso > paramMatrizAdj[j][i] + peso)
                    {       
                        paramListaVertices[i].indexAntecessor = j;
                        paramListaVertices[i].peso = paramMatrizAdj[j][i] + paramListaVertices[j].peso;
                        
                        novoNo = criarNoP(i, paramListaVertices[i].peso);
                        NoPesquisa = pesquisarNoP(NoRaiz, i);
                        if(NoPesquisa != NULL){
                            removerNoP(NoRaiz, NoPesquisa);
                        }
                        inserirInicioListaOrdenadaP(NoRaiz, novoNo);
                    }
                }
                
            }
        }
    }    
    
    NoP* NoSolucao = criarNoP(-1, -1);
    int indexAntecessor = paramIndexDestino;
    NoP* auxiliar = criarNoP(indexAntecessor, paramListaVertices[indexAntecessor].peso);
    inserirInicioListaP(NoSolucao, auxiliar);
    indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    
    while(indexAntecessor != -1){
        
        auxiliar = criarNoP(indexAntecessor, paramListaVertices[indexAntecessor].peso);
        inserirInicioListaP(NoSolucao, auxiliar);
        indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    }
    
    arquivoSaida = fopen("./arquivosDeSaida/dijkstraMatriz.txt", "a");
    fprintf(arquivoSaida, "Distância do caminho: %.2f\n", paramListaVertices[paramIndexDestino].peso);
    auxiliar = NoSolucao->prox;
    
    while(auxiliar != NULL){
        
        fprintf(arquivoSaida, "%d %.2f %d \n", paramListaVertices[auxiliar->id].id, paramListaVertices[auxiliar->id].peso, paramListaVertices[auxiliar->id].indexAntecessor == -1 ? 0 : paramListaVertices[paramListaVertices[auxiliar->id].indexAntecessor].id);
        auxiliar = auxiliar->prox;
    }
    
    fclose(arquivoSaida);    
}

/**Função verfica as partes conexas do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida com as partes conexas
 * @param paramListaVertices uma referência para um vertor de VerticeMatrizP onde seŕão armazenados os vértices
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @sa buscaProfundidadeMatrizP
 * @return
 */
void verificarPartesConexasMatrizP(char* paramNomeArquivoSaida, VerticeMatrizP * paramListaVertices, int* paramQtdVertices, float ** paramMatrizAdj)
{
    NoP* noConexos = criarNoP(-1, -1);
    int countConexos = 0, qtdVerticesBusca = 0, maxParteConexa = 0, minParteConexa = 0, maxQtdVerticeParteConexa = 0, minQtdVerticeParteConexa = *paramQtdVertices;
    FILE* saida;
    
    for(int i = 0; i < *paramQtdVertices; i++)
    {
        if(pesquisarNoP(noConexos, i)  == NULL)
        {
            qtdVerticesBusca = 0;
            countConexos++;
            buscaProfundidadeMatrizP(paramNomeArquivoSaida, i, -1, paramListaVertices, paramQtdVertices, paramMatrizAdj, noConexos, 0, &qtdVerticesBusca);
            
            if(maxQtdVerticeParteConexa < qtdVerticesBusca){
                maxQtdVerticeParteConexa = qtdVerticesBusca;
                maxParteConexa = countConexos;
            }
            
            if(minQtdVerticeParteConexa > qtdVerticesBusca){
                minQtdVerticeParteConexa = qtdVerticesBusca;
                minParteConexa = countConexos;
            }
            
            saida = fopen(paramNomeArquivoSaida, "a");
            if(saida == NULL)
            {
                printf("Método: verificarPartesConexasMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
                exit(0);
            }else
            {    
                fprintf(saida, "Parte: %d Quantidade de vértices: %d\n", countConexos, qtdVerticesBusca);
                fclose(saida);
            }
        }
    }
    
    saida = fopen(paramNomeArquivoSaida, "a");
    if(saida == NULL)
    {
        printf("Método: verificarPartesConexasMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(saida, "Quantidade de partes conexas %d\n", countConexos);
        fprintf(saida, "Maior Parte: %d Quantidade de Vértices %d\n",maxParteConexa ,maxQtdVerticeParteConexa);
        fprintf(saida, "Menor Parte: %d Quantidade de Vértices %d\n",minParteConexa ,minQtdVerticeParteConexa);
        fclose(saida);
    }
}
