/**
 * @brief "Esse código implementa uma lista encadeada simples"
 * @file lista.c
 * @author George Araújo - george.ifrn@gmail.com
 * @since 22/10/2016
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include "lista.h"

/**Função pesquisa No na lista a partir do No cabeça
 * @param noCabeca um ponteiro para um No que indica o início da lista
 * @param idBusca um inteiro que indica o identificador do No pesquisado
 * @sa 
 * @return um No com valor NULL se o No pesquisado não for encontrado ou com o No pesquisado, no caso dele existir na lista
 */
No* pesquisarNo(No* noCabeca, int idBusca)
{
    No* noAux = (No *)malloc(sizeof(No));
    noAux = noCabeca->prox;
    
    while(noAux != NULL)
    {
        
        if(noAux->id == idBusca)
        {
            break;
        }
        noAux = noAux->prox;
    }
    return noAux;
}

/**Função remove No do início de uma lista
 * @param noCabeca um ponteiro para um no que indica o início da lista
 * @sa 
 * @return o No removido
 */
No* removerNoInicio(No* noCabeca)
{
    No* noAux = noCabeca->prox;
    
    if(noAux != NULL)
    {
        noCabeca->prox = noAux->prox;
    }
    
    return noAux;
}

/**Função insere No no fim de uma lista
 * @param noCabeca um ponteiro para um No que indica o início da lista
 * @param novoNo um ponteiro para o novo No que será inserido
 * @sa 
 * @return
 */
void inserirFinalLista(No* noCabeca, No* novoNo)
{
    No* noAux = (No *)malloc(sizeof(No));
    noAux = noCabeca;
    while(noAux->prox != NULL)
    {
        noAux = noAux->prox;
    }
    noAux->prox = novoNo;
}

/**Função insere No no início de uma lista
 * @param noCabeca um ponteiro para um No que indica o início da lista
 * @param novoNo um ponteiro para o novo No que será inserido
 * @sa 
 * @return
 */
void inserirInicioLista(No* noCabeca, No* novoNo)
{
    novoNo->prox = noCabeca->prox;
    noCabeca->prox = novoNo;
}

/**Função aloca memória e cria um novo No
 * @param id um inteiro que será o identificador do No criado
 * @sa 
 * @return o No criado
 */
No* criarNo(int id)
{
    No* novoNo = (No *)malloc(sizeof(No));
    novoNo->id = id;
    novoNo->prox = NULL;
    
    return novoNo;
}