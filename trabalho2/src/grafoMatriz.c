/**
 * @brief "Esse código implementa uma biblioteca para grafos, simples não ponderados, representados por matriz de adjacência"
 * @file grafoMatriz.c
 * @author George Araújo - george.ifrn@gmail.com
 * @since 22/10/2016
 * @version 1.1
 */

#include <stdio.h>
#include <stdlib.h>
#include "lista.h"
#include "grafoMatriz.h"

/**Função imprime identificador e index de todos os Vértices do paramListaVertices
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return
 */
void imprimirVerticesMatriz(VerticeMatriz * paramListaVertices, int * paramMaxIndice)
{
    
    for(int i = 0; i <= *paramMaxIndice; i++)
    {
        printf("%d %d \n", paramListaVertices[i].id, i);
    }
}

/**Função imprime a matriz passada
 * @param paramMatrizAdj uma referência para um vertor de VerticeMatriz
 * @param paramQtdVertices um inteiro que indica o tamanho da matriz
 * @sa 
 * @return
 */
void imprimirMatrizAdjacencia(char ** paramMatrizAdj, int * paramQtdVertices)
{
    
    for(int i = 0; i < *paramQtdVertices; i++)
    {
        for(int j = 0; j < *paramQtdVertices; j++)
        {
            printf("%c ", paramMatrizAdj[i][j]);
        }
        printf("\n ");
    }
    
}

/**Função busca um VerticeMatriz no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz
 * @param paramVertice um inteiro com o identificador do VerticeMatriz procurado
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com valor -1 caso não tenha encontrado o VerticeMatriz ou  o index do vetor na posição onde o VerticeMatriz se encontra
 */
int buscarVerticeMatriz(VerticeMatriz * paramListaVertices, int paramVertice, int *paramMaxIndice)
{
    
    for(int i = 0; i <= *paramMaxIndice; i++)
    {
        if(paramListaVertices[i].id == paramVertice)
        {
            return i;
            
        }
    }
    
    return -1;
    
}


/**Função insere um VerticeMatriz no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz
 * @param paramVertice um inteiro com o identificador do novo VerticeMatriz
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com o novo tamanho usado do vetor paramListaVertices
 */
int inserirVerticeMatriz(VerticeMatriz * paramListaVertices, int paramVertice, int * paramMaxIndice)
{
    
    *paramMaxIndice = *paramMaxIndice + 1;
    
    paramListaVertices[*paramMaxIndice].id = paramVertice;
    paramListaVertices[*paramMaxIndice].grau = 0;
    paramListaVertices[*paramMaxIndice].nivel = 0;
    return *paramMaxIndice;
}

/**Função faz uma busca em largura no grafo a partir de um VerticeMatriz
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeMatriz que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeMatriz pai
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeMatriz já visitados
 * @param paramInicioFila um No cabeça para uma fila que indica ordem dos VerticeMatriz que devem ser visitados
 * @sa criarNo inserirInicioLista inserirFinalLista removerNoInicio pesquisarNo
 * @return
 */
void buscaLarguraMatriz(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeMatriz * paramListaVertices, int *paramQtdVertices, char ** paramMatrizAdj,No * paramNosVisitados, No * paramInicioFila)
{
    No *noVisitado;
    No *noAuxFila;
    No *novoNoFila;
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaLarguraMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {
        if(paramIndexPai == -1)
        {
            paramListaVertices[paramIndexRaiz].nivel = 0;
            paramListaVertices[paramIndexRaiz].indexAntecessor = -1;
            noVisitado = criarNo(paramIndexRaiz);
            inserirInicioLista(paramNosVisitados, noVisitado);
            
            fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, 0, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        }
        
        for(int i = 0; i < *paramQtdVertices; i++)
        {
            if(paramMatrizAdj[paramIndexRaiz][i] == '1')
            {    
                if(pesquisarNo(paramNosVisitados, i) == NULL)
                {    
                    noVisitado = criarNo(i);
                    inserirInicioLista(paramNosVisitados, noVisitado);

                    novoNoFila = criarNo(i);
                    inserirFinalLista(paramInicioFila, novoNoFila);
                    
                    paramListaVertices[i].nivel = paramListaVertices[paramIndexRaiz].nivel + 1;
                    paramListaVertices[i].indexAntecessor = paramIndexRaiz;
                    
                    fprintf(arquivoSaida, "%d %d %d \n", paramListaVertices[i].id,paramListaVertices[i].nivel, paramListaVertices[paramIndexRaiz].id);
                }
                    
            }
        }
        
        fclose(arquivoSaida);
        
        if(paramInicioFila->prox != NULL)
        {   
            noAuxFila = removerNoInicio(paramInicioFila);
            buscaLarguraMatriz(paramNomeArquivoSaida, noAuxFila->id, paramIndexRaiz,paramListaVertices, paramQtdVertices, paramMatrizAdj, paramNosVisitados, paramInicioFila);  
            
        }
    }
    
}

/**Função faz uma busca em profundidade no grafo a partir de um VerticeMatriz
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeMatriz que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeMatriz pai
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeMatriz já visitados
 * @param paramNivel um inteiro que indica o nivel do VerticeMatriz pai
 * @sa criarNo inserirInicioLista pesquisarNo
 * @return
 */
void buscaProfundidadeMatriz(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeMatriz * paramListaVertices, int* paramQtdVertices, char ** paramMatrizAdj, No * paramNosVisitados, int paramNivel, int* paramQtdVerticesBusca )
{
    No *noVisitado = criarNo(paramIndexRaiz);
    inserirInicioLista(paramNosVisitados, noVisitado);
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaProfundidadeMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, paramNivel, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        *paramQtdVerticesBusca = *paramQtdVerticesBusca + 1;
        fclose(arquivoSaida);
        paramNivel++;
        
        for(int i = 0; i < *paramQtdVertices; i++)
        {
            if(paramMatrizAdj[paramIndexRaiz][i] == '1')
            {    
                if(pesquisarNo(paramNosVisitados, i) == NULL){
                    
                    buscaProfundidadeMatriz(paramNomeArquivoSaida, i,paramIndexRaiz, paramListaVertices, paramQtdVertices, paramMatrizAdj, paramNosVisitados, paramNivel, paramQtdVerticesBusca);
                }
                    
            }
        }
    }
}

/**Função lê o arquivo de entrada com o grafo e gera o vetor de VerticeMatriz e a matriz de adjacência
 * @param paramNomeArquivoEntrada nome do arquivo de entrada do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida do grafo
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz onde seŕão armazenados os vértices
 * @sa buscarVerticeMatriz inserirVerticeMatriz 
 * @return
 */
void lerGrafoMatriz(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,char ** paramMatrizAdj, VerticeMatriz * paramListaVertices)
{
    
    FILE * entrada, * saida;
    int vertice1, vertice2, indexVertice1, indexVertice2, maxIndice = -1, qtdVertices = 0, qtdArestas = 0;
    
    entrada = fopen(paramNomeArquivoEntrada, "r");
    if(entrada == NULL)
    {
        printf("Método: lerGrafoMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoEntrada);
        exit(0);
    }else
    {
        fscanf(entrada, "%d",&qtdVertices);
        
        while(!feof(entrada))
        {
            fscanf(entrada, "%d %d", &vertice1, &vertice2);
            
            indexVertice1 = buscarVerticeMatriz(paramListaVertices, vertice1, &maxIndice);
            if(indexVertice1 == -1)
            {
                indexVertice1 = inserirVerticeMatriz(paramListaVertices, vertice1, &maxIndice);
            }
            
            indexVertice2 = buscarVerticeMatriz(paramListaVertices, vertice2, &maxIndice);
            if(indexVertice2 == -1)
            {
                indexVertice2 = inserirVerticeMatriz(paramListaVertices, vertice2, &maxIndice);
            }
            
            paramMatrizAdj[indexVertice1][indexVertice2] = '1';
            paramMatrizAdj[indexVertice2][indexVertice1] = '1';
            
            qtdArestas++;
            paramListaVertices[indexVertice2].grau++;
            paramListaVertices[indexVertice1].grau++;
            
        }
        
        fclose(entrada);
        entrada = NULL;
        free(entrada);
        maxIndice++;
        qtdArestas--;
        
        saida = fopen(paramNomeArquivoSaida, "w");
        fprintf(saida, "N = %d\n", maxIndice);
        fprintf(saida, "M = %d\n", qtdArestas);
        
        for(int i = 0; i < maxIndice; i++)
        {
            fprintf(saida, "%d %d\n", paramListaVertices[i].id, paramListaVertices[i].grau);                    
        }
        
        fclose(saida); 
        saida = NULL;
        free(saida);
        
    }
}

/**Função verfica as partes conexas do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida com as partes conexas
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz onde seŕão armazenados os vértices
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramMatrizAdj um matriz de adjacência que indica as arestas do grafo
 * @sa buscaProfundidadeMatriz 
 * @return
 */
void verificarPartesConexasMatriz(char* paramNomeArquivoSaida, VerticeMatriz * paramListaVertices, int* paramQtdVertices, char ** paramMatrizAdj)
{
    No* noConexos = criarNo(-1);
    int countConexos = 0, qtdVerticesBusca = 0, maxParteConexa = 0, minParteConexa = 0, maxQtdVerticeParteConexa = 0, minQtdVerticeParteConexa = *paramQtdVertices;
    FILE* saida;
    
    for(int i = 0; i < *paramQtdVertices; i++)
    {
        if(pesquisarNo(noConexos, i)  == NULL)
        {
            qtdVerticesBusca = 0;
            countConexos++;
            buscaProfundidadeMatriz(paramNomeArquivoSaida, i, -1, paramListaVertices, paramQtdVertices, paramMatrizAdj, noConexos, 0, &qtdVerticesBusca);
            
            if(maxQtdVerticeParteConexa < qtdVerticesBusca){
                maxQtdVerticeParteConexa = qtdVerticesBusca;
                maxParteConexa = countConexos;
            }
            
            if(minQtdVerticeParteConexa > qtdVerticesBusca){
                minQtdVerticeParteConexa = qtdVerticesBusca;
                minParteConexa = countConexos;
            }
            
            saida = fopen(paramNomeArquivoSaida, "a");
            if(saida == NULL)
            {
                printf("Método: verificarPartesConexasMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
                exit(0);
            }else
            {    
                fprintf(saida, "Parte: %d Quantidade de vértices: %d\n", countConexos, qtdVerticesBusca);
                fclose(saida);
            }
        }
    }
    
    saida = fopen(paramNomeArquivoSaida, "a");
    if(saida == NULL)
    {
        printf("Método: verificarPartesConexasMatriz - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(saida, "Quantidade de partes conexas %d\n", countConexos);
        fprintf(saida, "Maior Parte: %d Quantidade de Vértices %d\n",maxParteConexa ,maxQtdVerticeParteConexa);
        fprintf(saida, "Menor Parte: %d Quantidade de Vértices %d\n",minParteConexa ,minQtdVerticeParteConexa);
        fclose(saida);
    }
}

/**Função inicializa o indexAntecessor dos vertices para busca em largura
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz onde seŕão armazenados os vérticesparamQtdVertices
 * @param paramQtdVertices indica a quantidade de vértices do grafo
 * @sa
 * @return
 */
void prepararCalculoCaminhoMatriz(VerticeMatriz * paramListaVertices, int* paramQtdVertices)
{
    for(int h = 0; h < *paramQtdVertices; h++)
    {
        paramListaVertices[h].indexAntecessor = -1;
    }
    
}



/**Função calcula o caminho entre dois vertices a partir da busca em largura
 * @param paramIndexOrigem index do vértice de partida do caminho
 * @param paramIndexDestino index do vértice de chegada do caminho
 * @param paramListaVertices uma referência para um vertor de VerticeMatriz onde seŕão armazenados os vérticesparamQtdVertices
 * @param paramQtdVertices indica a quantidade de vértices do grafo
 * @sa inserirInicioLista criarNo buscaLarguraMatriz
 * @return
 */
void caminhoEntreVerticesMatriz(int paramIndexOrigem, int paramIndexDestino, VerticeMatriz * paramListaVertices, int* paramQtdVertices)
{
    FILE *arquivoSaida;
    No* NoSolucao = criarNo(-1);
    int distancia = 0;
    int indexAntecessor = paramIndexDestino;
    No* auxiliar = criarNo(indexAntecessor);
    inserirInicioLista(NoSolucao, auxiliar);
    indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    
    while(indexAntecessor != -1){
        distancia++;
        auxiliar = criarNo(indexAntecessor);
        inserirInicioLista(NoSolucao, auxiliar);
        indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    }
    
    arquivoSaida = fopen("./arquivosDeSaida/caminhoBuscaLarguraMatriz.txt", "a");
    fprintf(arquivoSaida, "Distância do caminho: %d\n", distancia);
    auxiliar = NoSolucao->prox;
    
    while(auxiliar != NULL){
        
        fprintf(arquivoSaida, "%d %d \n", paramListaVertices[auxiliar->id].id, paramListaVertices[auxiliar->id].indexAntecessor == -1.0 ? 0 : paramListaVertices[paramListaVertices[auxiliar->id].indexAntecessor].id);
        auxiliar = auxiliar->prox;
    }
    
    fclose(arquivoSaida);    
}