/**
 * @brief "Esse código implementa uma lista encadeada simples com peso nos nós"
 * @file listaPonderada.c
 * @author George Araújo - george.ifrn@gmail.com
 * @since 22/11/2016
 * @version 1.0
 */

#include <stdio.h>
#include <stdlib.h>
#include "listaPonderada.h"

/**Função imprime os Ids dos elementos da lista
 * @param noCabeca um ponteiro para um NoP que indica o início da lista
 * @sa 
 * @return
 */
void imprimirListaP(NoP* noCabeca)
{
    NoP* noAux = (NoP *)malloc(sizeof(NoP));
    noAux = noCabeca->prox;
    
    while(noAux != NULL)
    {
        printf("%d - ", noAux->id);
        noAux = noAux->prox;
    }
    printf("\n");
}

/**Função pesquisa NoP na lista a partir do NoP cabeça
 * @param noCabeca um ponteiro para um NoP que indica o início da lista
 * @param idBusca um inteiro que indica o identificador do NoP pesquisado
 * @sa 
 * @return um NoP com valor NULL se o NoP pesquisado não for encontrado ou com o NoP pesquisado, no caso dele existir na lista
 */
NoP* pesquisarNoP(NoP* noCabeca, int idBusca)
{
    NoP* noAux = (NoP *)malloc(sizeof(NoP));
    noAux = noCabeca->prox;
    
    while(noAux != NULL)
    {
        
        if(noAux->id == idBusca)
        {
            break;
        }
        noAux = noAux->prox;
    }
    return noAux;
}

/**Função pesquisa NoP na lista a partir do No cabeça e o remove
 * @param noCabeca um ponteiro para um NoP que indica o início da lista
 * @param idBusca um inteiro que indica o identificador do NoP que deve ser removido
 * @sa 
 * @return um NoP com valor NULL se o NoP pesquisado não for encontrado ou com o NoP pesquisado, no caso dele existir na lista
 */
NoP* removerNoP(NoP* noCabeca, int idBusca)
{
    NoP* noAux, *noRetorno;
    noAux = noCabeca;
    
    while(noAux->prox != NULL)
    {
        
        if(noAux->prox->id == idBusca)
        {
            noRetorno = noAux->prox;
            noAux->prox = noRetorno->prox;
            break;
        }
        noAux = noAux->prox;
    }
    noRetorno = noAux->prox;
    return noRetorno;
}

/**Função remove NoP do início de uma lista
 * @param noCabeca um ponteiro para um no que indica o início da lista
 * @sa 
 * @return o NoP removido
 */
NoP* removerNoInicioP(NoP* noCabeca)
{
    NoP* noAux = noCabeca->prox;
    
    if(noAux != NULL)
    {
        noCabeca->prox = noAux->prox;
    }
    
    return noAux;
}

/**Função insere NoP no fim de uma lista
 * @param noCabeca um ponteiro para um NoP que indica o início da lista
 * @param novoNo um ponteiro para o novo NoP que será inserido
 * @sa 
 * @return
 */
void inserirFinalListaP(NoP* noCabeca, NoP* novoNo)
{
    NoP* noAux = (NoP *)malloc(sizeof(NoP));
    noAux = noCabeca;
    while(noAux->prox != NULL)
    {
        noAux = noAux->prox;
    }
    noAux->prox = novoNo;
}

/**Função insere NoP no início de uma lista
 * @param noCabeca um ponteiro para um NoP que indica o início da lista
 * @param novoNo um ponteiro para o novo NoP que será inserido
 * @sa 
 * @return
 */
void inserirInicioListaP(NoP* noCabeca, NoP* novoNo)
{
    novoNo->prox = noCabeca->prox;
    noCabeca->prox = novoNo;
}

/**Função insere NoP na lista ordenada
 * @param noCabeca um ponteiro para um NoP que indica o início da lista
 * @param novoNo um ponteiro para o novo NoP que será inserido
 * @sa 
 * @return
 */
void inserirInicioListaOrdenadaP(NoP* noCabeca, NoP* novoNo)
{
    NoP* auxiliar = noCabeca;
    while(auxiliar->prox != NULL)
    {
        if(auxiliar->prox->peso > novoNo->peso)
        {
            break;
        }
        auxiliar = auxiliar->prox;
    }
    novoNo->prox = auxiliar->prox;
    auxiliar->prox = novoNo;
}

/**Função aloca memória e cria um novo NoP
 * @param id um inteiro que será o identificador do NoP criado
 * @sa 
 * @return o NoP criado
 */
NoP* criarNoP(int id, float peso)
{
    NoP* novoNo = (NoP *)malloc(sizeof(NoP));
    novoNo->id = id;
    novoNo->peso = peso;
    novoNo->prox = NULL;
    
    return novoNo;
}