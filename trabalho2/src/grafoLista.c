/**
 * @brief "Esse código implementa uma biblioteca para grafos, simples não ponderados, representados por lista de adjacência"
 * @file grafoLista.c
 * @author George Araújo - george.ifrn@gmail.com
 * @since 22/10/2016
 * @version 1.1
 */

#include <stdio.h>
#include <stdlib.h>
#include "lista.h"
#include "grafoLista.h"

/**Função imprime identificador de todos os Vértices do paramListaVertices e sua lista de adjacência
 * @param paramListaVertices uma referência para um vertor de VerticeLista
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return
 */
void imprimirVerticesLista(VerticeLista * paramListaVertices, int * paramMaxIndice)
{
    No* aux;
    for(int i = 0; i < *paramMaxIndice; i++)
    {
        printf("%d -> ", paramListaVertices[i].id);
        aux = paramListaVertices[i].aresta;
        while(aux->prox != NULL)
        {
            aux = aux->prox;
            printf("%d ->", paramListaVertices[aux->id].id);
        }
        printf("\n");
    }
}

/**Função busca um VerticeLista no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeLista
 * @param paramVertice um inteiro com o identificador do VerticeLista procurado
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com valor -1 caso não tenha encontrado o VerticeLista ou  o index do vetor na posição onde o VerticeLista se encontra
 */
int buscarVerticeLista(VerticeLista * paramListaVertices, int paramVertice, int *paramMaxIndice)
{
    
    for(int i = 0; i <= *paramMaxIndice; i++)
    {
        if(paramListaVertices[i].id == paramVertice)
        {
            return i;
            
        }
    }
    
    return -1;
    
}

/**Função insere um VerticeLista no vetor
 * @param paramListaVertices uma referência para um vertor de VerticeLista
 * @param paramVertice um inteiro com o identificador do novo VerticeLista
 * @param paramMaxIndice um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa 
 * @return um inteiro com o novo tamanho usado do vetor paramListaVertices
 */
int inserirVerticeLista(VerticeLista* paramListaVertices, int paramVertice, int * paramMaxIndice)
{
    
    *paramMaxIndice = *paramMaxIndice + 1;
    
    paramListaVertices[*paramMaxIndice].id = paramVertice;
    paramListaVertices[*paramMaxIndice].grau = 0;
    paramListaVertices[*paramMaxIndice].nivel = 0;
    paramListaVertices[*paramMaxIndice].aresta = criarNo(-1);
    return *paramMaxIndice;
}

/**Função faz uma busca em largura no grafo a partir de um VerticeLista
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeLista que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeLista pai
 * @param paramListaVertices uma referência para um vertor de VerticeLista
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeLista já visitados
 * @param paramInicioFila um No cabeça para uma fila que indica ordem dos VerticeLista que devem ser visitados
 * @sa criarNo inserirInicioLista inserirFinalLista removerNoInicio pesquisarNo
 * @return
 */
void buscaLarguraLista(char * paramNomeArquivoSaida, int paramIndexRaiz, int paramIndexPai,VerticeLista * paramListaVertices, int *paramQtdVertices,No * paramNosVisitados, No * paramInicioFila)
{
    No *noVisitado;
    No *noAuxFila;
    No *novoNoFila;
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaLarguraLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {
        if(paramIndexPai == -1)
        {
            paramListaVertices[paramIndexRaiz].nivel = 0;
            paramListaVertices[paramIndexRaiz].indexAntecessor = -1;
            noVisitado = criarNo(paramIndexRaiz);
            inserirInicioLista(paramNosVisitados, noVisitado);
            fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, 0, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        }
        
        noAuxFila = paramListaVertices[paramIndexRaiz].aresta;
        while(noAuxFila->prox != NULL)
        {
            noAuxFila = noAuxFila->prox;
            
            if(pesquisarNo(paramNosVisitados, noAuxFila->id) == NULL)
            {    
                noVisitado = criarNo(noAuxFila->id);
                inserirInicioLista(paramNosVisitados, noVisitado);

                novoNoFila = criarNo(noAuxFila->id);
                inserirFinalLista(paramInicioFila, novoNoFila);
                
                paramListaVertices[noAuxFila->id].nivel = paramListaVertices[paramIndexRaiz].nivel + 1;
                paramListaVertices[noAuxFila->id].indexAntecessor = paramIndexRaiz;
                
                fprintf(arquivoSaida, "%d %d %d \n", paramListaVertices[noAuxFila->id].id,paramListaVertices[noAuxFila->id].nivel, paramListaVertices[paramIndexRaiz].id);
            }
                    
            
        }
        
        fclose(arquivoSaida);
        
        if(paramInicioFila->prox != NULL)
        {   
            noAuxFila = removerNoInicio(paramInicioFila);
            buscaLarguraLista(paramNomeArquivoSaida, noAuxFila->id, paramIndexRaiz,paramListaVertices, paramQtdVertices, paramNosVisitados, paramInicioFila);  
            
        }
    }
    
}

/**Função faz uma busca em profundidade no grafo a partir de um VerticeLista
 * @param paramNomeArquivoSaida nome do arquivo de saida da busca
 * @param paramIndexRaiz index do VerticeLista que deve ser inserido no arquivo e explorado
 * @param paramIndexPai index do VerticeLista pai
 * @param paramListaVertices uma referência para um vertor de VerticeLista
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @param paramNosVisitados um No cabeça para uma lista dos VerticeLista já visitados
 * @param paramNivel um inteiro que indica o nivel do VerticeLista pai
 * @sa criarNo inserirInicioLista pesquisarNo
 * @return
 */
void buscaProfundidadeLista(char* paramNomeArquivoSaida, int paramIndexRaiz,int paramIndexPai, VerticeLista * paramListaVertices, int* paramQtdVertices, No * paramNosVisitados, int paramNivel, int* paramQtdVerticesBusca)
{
    No *noVisitado = criarNo(paramIndexRaiz);
    No *noAux;
    inserirInicioLista(paramNosVisitados, noVisitado);
    FILE * arquivoSaida;
    
    arquivoSaida = fopen(paramNomeArquivoSaida, "a");
    if(arquivoSaida == NULL)
    {
        printf("Método: buscaProfundidadeLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(arquivoSaida, "%d %d %d\n", paramListaVertices[paramIndexRaiz].id, paramNivel, paramIndexPai == -1 ? -1 : paramListaVertices[paramIndexPai].id);
        *paramQtdVerticesBusca = *paramQtdVerticesBusca + 1;
        fclose(arquivoSaida);
        paramNivel++;
        noAux = paramListaVertices[paramIndexRaiz].aresta;
            
        while(noAux->prox != NULL)
        {
            noAux = noAux->prox;
            if(pesquisarNo(paramNosVisitados, noAux->id) == NULL){
                buscaProfundidadeLista(paramNomeArquivoSaida, noAux->id,paramIndexRaiz, paramListaVertices, paramQtdVertices, paramNosVisitados, paramNivel, paramQtdVerticesBusca);
            }
        }
    }
}

/**Função lê o arquivo de entrada com o grafo e gera o vetor de VerticeLista
 * @param paramNomeArquivoEntrada nome do arquivo de entrada do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida do grafo
 * @param paramListaVertices uma referência para um vertor de VerticeLista onde seŕão armazenados os vértices
 * @sa buscarVerticeLista inserirVerticeLista inserirFinalLista
 * @return
 */
void lerGrafoLista(char * paramNomeArquivoEntrada, char * paramNomeArquivoSaida,VerticeLista* paramListaVertices)
{
    FILE * entrada, * saida;
    int vertice1, vertice2, indexVertice1, indexVertice2, maxIndice = -1, qtdVertices = 0, qtdArestas = 0;
    No* noAux;
    entrada = fopen(paramNomeArquivoEntrada, "r");
    if(entrada == NULL)
    {
        printf("Método: lerGrafoLista - Erro na leitura do arquivo %s\n", paramNomeArquivoEntrada);
        exit(0);
    }else
    {
        fscanf(entrada, "%d",&qtdVertices);
        
        while(!feof(entrada))
        {
            fscanf(entrada, "%d %d", &vertice1, &vertice2);
            
            
            indexVertice1 = buscarVerticeLista(paramListaVertices, vertice1, &maxIndice);
            if(indexVertice1 == -1)
            {
                indexVertice1 = inserirVerticeLista(paramListaVertices, vertice1, &maxIndice);
            }
            
            indexVertice2 = buscarVerticeLista(paramListaVertices, vertice2, &maxIndice);
            if(indexVertice2 == -1)
            {
                indexVertice2 = inserirVerticeLista(paramListaVertices, vertice2, &maxIndice);
            }
            
            noAux = criarNo(indexVertice1);
            inserirFinalLista(paramListaVertices[indexVertice2].aresta, noAux);
            noAux = criarNo(indexVertice2);
            inserirFinalLista(paramListaVertices[indexVertice1].aresta, noAux);
                        
            qtdArestas++;
            paramListaVertices[indexVertice2].grau++;
            paramListaVertices[indexVertice1].grau++;
            
        }
        
        fclose(entrada);
        entrada = NULL;
        free(entrada);
        maxIndice++;
        qtdArestas--;
        
        saida = fopen(paramNomeArquivoSaida, "w");
        fprintf(saida, "N = %d\n", maxIndice);
        fprintf(saida, "M = %d\n", qtdArestas);
        
        for(int i = 0; i < maxIndice; i++)
        {
            fprintf(saida, "%d %d\n", paramListaVertices[i].id, paramListaVertices[i].grau);                    
        }
        
        fclose(saida); 
        saida = NULL;
        free(saida);
        
    }
}

/**Função verfica as partes conexas do grafo
 * @param paramNomeArquivoSaida nome do arquivo de saida com as partes conexas
 * @param paramListaVertices uma referência para um vertor de VerticeLista onde seŕão armazenados os vértices
 * @param paramQtdVertices um inteiro que indica o tamanho do vetor paramListaVertices
 * @sa pesquisarNo buscaProfundidadeLista
 * @return
 */
void verificarPartesConexasLista(char* paramNomeArquivoSaida, VerticeLista * paramListaVertices, int* paramQtdVertices)
{
    No* noConexos = criarNo(-1);
    int countConexos = 0, maxParteConexa = 0, minParteConexa = 0, maxQtdVerticeParteConexa = 0, minQtdVerticeParteConexa = *paramQtdVertices;
    FILE* saida;
    int qtdVerticesBusca = 0;
    
    for(int i = 0; i < *paramQtdVertices; i++)
    {
        if(pesquisarNo(noConexos, i)  == NULL)
        {
            countConexos++;
            qtdVerticesBusca = 0;
            buscaProfundidadeLista(paramNomeArquivoSaida, i, -1, paramListaVertices, paramQtdVertices, noConexos, 0, &qtdVerticesBusca);
            
            if(maxQtdVerticeParteConexa < qtdVerticesBusca){
                maxQtdVerticeParteConexa = qtdVerticesBusca;
                maxParteConexa = countConexos;
            }
            
            if(minQtdVerticeParteConexa > qtdVerticesBusca){
                minQtdVerticeParteConexa = qtdVerticesBusca;
                minParteConexa = countConexos;
            }
            
            saida = fopen(paramNomeArquivoSaida, "a");
            if(saida == NULL)
            {
                printf("Método: verificarPartesConexasLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
                exit(0);
            }else
            {    
                fprintf(saida, "Parte: %d Quantidade de vértices: %d\n", countConexos, qtdVerticesBusca);
                fclose(saida);
            }
        }
    }
    
    saida = fopen(paramNomeArquivoSaida, "a");
    if(saida == NULL)
    {
        printf("Método: verificarPartesConexasLista - Erro na leitura do arquivo %s\n", paramNomeArquivoSaida);
        exit(0);
    }else
    {    
        fprintf(saida, "Quantidade de partes conexas %d\n", countConexos);
        fprintf(saida, "Maior Parte: %d Quantidade de Vértices %d\n",maxParteConexa ,maxQtdVerticeParteConexa);
        fprintf(saida, "Menor Parte: %d Quantidade de Vértices %d\n",minParteConexa ,minQtdVerticeParteConexa);
        fclose(saida);
    }
}

/**Função verfica a quantidade de vértices indicada no arquivo de entrada
 * @param paramNomeArquivoEntrada nome do arquivo de entrada do grafo
 * @param paramQtdVertices uma referência para inteiro que irá guardar a quantidade de Vértices
 * @sa 
 * @return
 */
void lerQuantidadeVertices(char* paramNomeArquivoEntrada, int* paramQtdVertices)
{
    FILE * entrada;
    
    entrada = fopen(paramNomeArquivoEntrada, "r");
    if(entrada == NULL)
    {
        printf("Método: lerQuantidadeVertices - Erro na leitura do arquivo %s\n", paramNomeArquivoEntrada);
        exit(0);
        
    }else
    {    
        fscanf(entrada, "%d",paramQtdVertices);
        
        fclose(entrada);
    }
    entrada = NULL;
    free(entrada);
}


/**Função inicializa o indexAntecessor dos vertices para busca em largura
 * @param paramListaVertices uma referência para um vertor de VerticeLista onde seŕão armazenados os vérticesparamQtdVertices
 * @param paramQtdVertices indica a quantidade de vértices do grafo
 * @sa
 * @return
 */
void prepararCalculoCaminhoLista(VerticeLista * paramListaVertices, int* paramQtdVertices)
{
    for(int h = 0; h < *paramQtdVertices; h++)
    {
        paramListaVertices[h].indexAntecessor = -1;
    }
}



/**Função calcula o caminho entre dois vertices a partir da busca em largura
 * @param paramIndexOrigem index do vértice de partida do caminho
 * @param paramIndexDestino index do vértice de chegada do caminho
 * @param paramListaVertices uma referência para um vertor de VerticeLista onde seŕão armazenados os vérticesparamQtdVertices
 * @param paramQtdVertices indica a quantidade de vértices do grafo
 * @sa inserirInicioLista criarNo buscaLarguraLista
 * @return
 */
void caminhoEntreVerticesLista(int paramIndexOrigem, int paramIndexDestino, VerticeLista * paramListaVertices, int* paramQtdVertices)
{
    FILE * arquivoSaida;
    No* NoSolucao = criarNo(-1);
    int distancia = 0;
    int indexAntecessor = paramIndexDestino;
    No* auxiliar = criarNo(indexAntecessor);
    inserirInicioLista(NoSolucao, auxiliar);
    indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    
    while(indexAntecessor != -1){
        distancia++;
        auxiliar = criarNo(indexAntecessor);
        inserirInicioLista(NoSolucao, auxiliar);
        indexAntecessor = paramListaVertices[indexAntecessor].indexAntecessor;
    }
    
    arquivoSaida = fopen("./arquivosDeSaida/caminhoBuscaLarguraLista.txt", "a");
    fprintf(arquivoSaida, "Distância do caminho: %d\n", distancia);
    auxiliar = NoSolucao->prox;
    
    while(auxiliar != NULL){
        
        fprintf(arquivoSaida, "%d %d \n", paramListaVertices[auxiliar->id].id, paramListaVertices[auxiliar->id].indexAntecessor == -1.0 ? 0 : paramListaVertices[paramListaVertices[auxiliar->id].indexAntecessor].id);
        auxiliar = auxiliar->prox;
    }
    
    fclose(arquivoSaida);    
}